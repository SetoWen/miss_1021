#include "sd.h"
#include "bsp_sd_fatfs_test.h"
#include "ff.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "picture.h"

#define menu_max 5
#define file_max 20
char menu_filename[menu_max][10];//此数组用作显示窗口数组，既最多显示menu_max个文件
char all_filename[file_max][15];//该数组存储根目录下所有的.txt文件，最多支持file_max个
uint8_t sdcard_flag=0;
union float2byte
{
	uint8_t byte_t[4];
	float t;
}trans;
uint8_t sd_save[2048];
char path[10];//10个文件的路径地址
uint16_t sd_num=0;
//void sd_control(void)
//{
//	uint8_t dat=0;
//	sd_num=0;
//	for(int i=0;i<70;i++)
//  {
//    for(int j=0;j<144;j+=8)
//    {
//      dat=0;
//      if(image_binaryzation[i][j+0]>1)dat|=0x01;
//      if(image_binaryzation[i][j+1]>1)dat|=0x02;
//      if(image_binaryzation[i][j+2]>1)dat|=0x04;
//      if(image_binaryzation[i][j+3]>1)dat|=0x08;
//      if(image_binaryzation[i][j+4]>1)dat|=0x10;
//      if(image_binaryzation[i][j+5]>1)dat|=0x20;
//      if(image_binaryzation[i][j+6]>1)dat|=0x40;
//      if(image_binaryzation[i][j+7]>1)dat|=0x80;
//	  sd_save[sd_num++]=dat;
//    }
//  }
//	W_SD=sdhc_writeblocks(sd_save,sector_old,4);//image_binaryzation
//	//LCD_ShowNum(0,0,write_num,8);
//	sector_old+=4;
//	write_num++;
//}
void Save_filename(char *filename_t,char *fn)
{
	while(*fn!='\0')
	{
		*filename_t=*fn;
		fn++;
	}
}

FATFS fatfs_sd;//创建一个文件系统对象结构体
FIL fil_sd;//创建一个文件对象结构体
FRESULT rc;//定义一个函数类型返回值
UINT bw;//写入的字节数
FILINFO fileinfo;
DIR dir1;
uint8_t *fname[10];
uint8_t sttr[10];
uint16_t file_size;
uint8_t fatfs_SD_init(void)//返回rc的值为0，成功
{
	uint8_t str[20];
	rc=f_mount(0,&fatfs_sd);//挂载一个（逻辑）磁盘工作区0，
	sprintf(str,"%d.TXT",0);
	rc=f_open(&fil_sd,str,FA_CREATE_ALWAYS | FA_WRITE);//初始化sd卡
//	rc=fatfs_file_select();
	return rc;

	
}
char file_name[15];//存放文件名
uint32_t image_num=0;//图像文件序号
uint8_t str2[10];
uint8_t fatfs_file_select(void)
{
	char *fn;
	int i=0,j=0;
	uint8_t clear_flag=0,new_flag=0,out_flag=0;
	//读取根目录下所有文件
//	while(Key_value==KEY2){}
//	LCD_Clear(WHITE);
		if(f_opendir(&dir1,(const TCHAR*)"0:")==FR_OK)//打开一个目录
	{
		while(f_readdir(&dir1, &fileinfo)==FR_OK)
		{
			if(fileinfo.fname[0] == 0)//读取到末尾了，退出读取
			{
				i=j=0;
				break;
			}
	#if _USE_LFN
						fn = *fileinfo.lfname ? fileinfo.lfname : fileinfo.fname;
	#else							   
						fn = fileinfo.fname;
	#endif
			//注意图像图像名字大小不能超过15个字符
			if(strstr(fn,".TXT")!=NULL || strstr(fn,".txt")!=NULL)//只查找指定后缀.TXT或.txt的文件
			{
				//将当前文件名称拷贝到全局数组中

				f_open(&fil_sd,fn,FA_READ);
				file_size=f_size(&fil_sd)/2048;
				f_close(&fil_sd);
				strcpy(all_filename[j],fn);
				LCD_ShowString(0,i,all_filename[j++]); 
				sprintf((char *)str2,"size: %d",file_size);
				LCD_ShowString(120,i,str2);
				i=i+16;
				
			}
		}
		
		LCD_ShowString(10,210,"Clear");
		LCD_ShowString(200,210,"New");
//		while(1)
//		{
//			if(Key_value==KEY1)
//			{
//				systick_delay_ms(20);
//				if(Key_value==KEY1)
//				{
//					clear_flag=1;
//					break;
//				}
//			}
//			if(Key_value==KEY2)
//			{
//				systick_delay_ms(20);
//				if(Key_value==KEY2)
//				{
//					new_flag=1;
//					break;
//				}
//			}
//			if(Key_value!=KEY2 && Key_value!=KEY1 && Key_value!=7)
//			{
//				out_flag=1;
//				break;
//			}
//		}
		if(out_flag==0)
		{
			int  k=0;
			while(all_filename[k][0]!='\0')
			{
				k++;
			}
			if(clear_flag==1)//清除目录下所有的txt文件，并重新建一个文件
			{
				clear_flag=0;
				while(k>0)
				{
					sprintf((char*)file_name, "%s/%s", "0:", all_filename[--k]);
					rc=f_unlink(file_name);
				}
				sprintf(file_name,"%d.TXT",++k);
				rc=f_open(&fil_sd,file_name,FA_CREATE_ALWAYS | FA_WRITE);//初始化sd卡
				f_sync(&fil_sd);
				if(fatfs_sd.fs_type==0)//如果挂载文件系统失败
				{
					fil_sd.fs=0;//重置文件对象的指针
					return rc;
				}
			}
			if(new_flag==1)
			{
				new_flag=0;
				sprintf(file_name,"%d.TXT",++k);
				rc=f_open(&fil_sd,file_name,FA_CREATE_ALWAYS | FA_WRITE);//初始化sd卡
				f_sync(&fil_sd);
				if(fatfs_sd.fs_type==0)//如果挂载文件系统失败
				{
					fil_sd.fs=0;//重置文件对象的指针
					return rc;
				}
			}
		}
	}
	else
	{
		while(1)
		{
			LCD_ShowString(0,120,"Open filedir error!");
		}
	}
//	LCD_Clear(WHITE);
	//以下是为了刷新一下当前文件夹的文件，方便确认操作是否成功
	if(out_flag==0 &&f_opendir(&dir1,(const TCHAR*)"0:")==FR_OK)//打开一个目录
	{
		while(f_readdir(&dir1, &fileinfo)==FR_OK)
		{
			if(fileinfo.fname[0] == 0)//读取到末尾了，退出读取
			{
				i=j=0;
				break;
			}
	#if _USE_LFN
						fn = *fileinfo.lfname ? fileinfo.lfname : fileinfo.fname;
	#else							   
						fn = fileinfo.fname;
	#endif
			//注意图像图像名字大小不能超过15个字符
			if(strstr(fn,".TXT")!=NULL || strstr(fn,".txt")!=NULL)//只查找指定后缀.TXT或.txt的文件
			{
				//将当前文件名称拷贝到全局数组中
				strcpy(all_filename[j],fn);
				LCD_ShowString(0,i,(const uint8_t*)all_filename[j++]); 
				i=i+16;
			}
		}
	}
	if(clear_flag)
	{
		LCD_ShowString(0,120,"Clear All File Success!");
	}
	else if(new_flag)
	{
		LCD_ShowString(0,120,"Create New File Success!");
	}
	if(clear_flag || new_flag)
	{
		systick_delay_ms(500);
	}
//	LCD_Clear(WHITE);
	while(rc);//当rc等于0（即创建成功）时，条件为假，跳出循环，如果是其他返回值就再次新建
	return rc;
}
//选择录边缘图像还是录二值化图像
#define USE_SOBEL
void fatfs_SD_control(void)
{		
	uint8_t dat=0;
	uint16_t sd_num=0;
	
	if(fil_sd.fs!=0)//如果文件对象指针指向某个文件，则开始写
	{	
		
		for(int i=0;i<70;i++)
		{
			for(int j=0;j<144;j=j+8)
			{
				dat=0;
				#ifdef USE_SOBEL
				if(image[i][j+0]>1)dat|=0x01;
				if(image[i][j+1]>1)dat|=0x02;
				if(image[i][j+2]>1)dat|=0x04;
				if(image[i][j+3]>1)dat|=0x08;
				if(image[i][j+4]>1)dat|=0x10;
				if(image[i][j+5]>1)dat|=0x20;
				if(image[i][j+6]>1)dat|=0x40;
				if(image[i][j+7]>1)dat|=0x80;
				#else
				if(image_IterativeThSegment[i][j+0]>1)dat|=0x01;
				if(image_IterativeThSegment[i][j+1]>1)dat|=0x02;
				if(image_IterativeThSegment[i][j+2]>1)dat|=0x04;
				if(image_IterativeThSegment[i][j+3]>1)dat|=0x08;
				if(image_IterativeThSegment[i][j+4]>1)dat|=0x10;
				if(image_IterativeThSegment[i][j+5]>1)dat|=0x20;
				if(image_IterativeThSegment[i][j+6]>1)dat|=0x40;
				if(image_IterativeThSegment[i][j+7]>1)dat|=0x80;
				#endif
				sd_save[sd_num++]=dat;
			}
		}
//		//70*144/8=1260个uint8_t数组
//		for(int i=0;i<70;i++)
//		{
//			sd_save[sd_num++]=Left_Black_boundary[i];
//		}
//		for(int i=0;i<70;i++)
//		{
//			sd_save[sd_num++]=Right_Black_boundary[i];
//		}
//		for(int i=0;i<70;i++)
//		{
//			sd_save[sd_num++]=Middle_line[i];
//		}
//		sd_save[sd_num++]=(uint8_t)Right_angle_pointX_1;
//		sd_save[sd_num++]=(uint8_t)Right_angle_pointY_1;
//		sd_save[sd_num++]=(uint8_t)0;
//		sd_save[sd_num++]=(uint8_t)0;
//		sd_save[sd_num++]=(uint8_t)Right_angle_pointX_3;
//		sd_save[sd_num++]=(uint8_t)Right_angle_pointY_3;
//		sd_save[sd_num++]=(uint8_t)Left_angle_pointX_1;
//		sd_save[sd_num++]=(uint8_t)Left_angle_pointY_1;
//		sd_save[sd_num++]=(uint8_t)0;
//		sd_save[sd_num++]=(uint8_t)0;
//		sd_save[sd_num++]=(uint8_t)Left_angle_pointX_3;
//		sd_save[sd_num++]=(uint8_t)Left_angle_pointY_3;
//		//70*3+1260=1470
//		for(int i=0;i<6;i++)
//		{
//			sd_save[sd_num++]=Lcircle_pointy[i];
//		}
//		for(int i=0;i<6;i++)
//		{
//			sd_save[sd_num++]=Lcircle_pointx[i];
//		}
//		sd_save[sd_num++]=cross_flag;
//		sd_save[sd_num++]=cross_process;
//		sd_save[sd_num++]=circle_flag;
//		sd_save[sd_num++]=circle_process;
//		sd_save[sd_num++]=Big_circle_flag;
//		sd_save[sd_num++]=Encoder_flag;
//		sd_save[sd_num++]=outside_flag;
//		
//		trans.t=Fuzzy_Servo.Dpos;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Pos;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[0];
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[0]>>8;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[0]>>16;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[0]>>24;
//		
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[1];
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[1]>>8;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[1]>>16;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Dpos_Rule[1]>>24;
//		
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[0];
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[0]>>8;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[0]>>16;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[0]>>24;
//		
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[1];
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[1]>>8;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[1]>>16;
//		sd_save[sd_num++]=(uint8_t)Fuzzy_Servo.Pos_Rule[1]>>24;
//		
//		trans.t=Fuzzy_Servo.Dpos_U[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Dpos_U[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Pos_U[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Pos_U[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Fout;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=Fuzzy_Servo.Fout_u;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		sd_save[sd_num++]=(uint8_t)lost_left_first;
//		sd_save[sd_num++]=(uint8_t)lost_left_end;
//		sd_save[sd_num++]=(uint8_t)lost_right_first;
//		sd_save[sd_num++]=(uint8_t)lost_right_end;
//		
//		sd_save[sd_num++]=(uint8_t)useful_line;
//		sd_save[sd_num++]=(uint8_t)L_speed;
//		sd_save[sd_num++]=(uint8_t)R_speed;

//		sd_save[sd_num++]=(uint8_t)Big_circle_flag;
//		sd_save[sd_num++]=(uint8_t)Ultrasonic_distance;
//		
//		trans.t=k_r1;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=k_l1;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		sd_save[sd_num++]=(uint8_t)Trend_Left;
//		sd_save[sd_num++]=(uint8_t)Trend_Right;
//		for(int i=0;i<6;i++)
//		{
//			sd_save[sd_num++]=Rcircle_pointy[i];
//		}
//		for(int i=0;i<6;i++)
//		{
//			sd_save[sd_num++]=Rcircle_pointx[i];
//		}
//		sd_save[sd_num++]=(uint8_t)(cycle_time/100);
//		
//		sd_save[sd_num++]=(uint8_t)Ramp_flag;
//		sd_save[sd_num++]=(uint8_t)Block_flag;
//		sd_save[sd_num++]=(uint8_t)Block_in;
//		sd_save[sd_num++]=(uint8_t)Block_out;
//		sd_save[sd_num++]=(uint8_t)(Encoder_Sumvalue);
//		sd_save[sd_num++]=(uint8_t)(Encoder_Sumvalue>>8);
//		sd_save[sd_num++]=(uint8_t)(Encoder_Sumvalue>>16);
//		sd_save[sd_num++]=(uint8_t)(Encoder_Sumvalue>>24);
//		
//		sd_save[sd_num++]=(uint8_t)EM_right_value;
//		sd_save[sd_num++]=(uint8_t)EM_left_value;
//		sd_save[sd_num++]=(uint8_t)Track_start;
//		
//		trans.t=EM_error;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=EM_derror;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=EM_out;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		sd_save[sd_num++]=(uint8_t)road_lost_flag;
//		sd_save[sd_num++]=(uint8_t)road_lost_direction;
//		
//		sd_save[sd_num++]=(uint8_t)circle_X;
//		sd_save[sd_num++]=(uint8_t)circle_Y;
//		
//		sd_save[sd_num++]=(uint8_t)accelerate_flag;
//		sd_save[sd_num++]=(uint8_t)slow_down_flag;
//		sd_save[sd_num++]=(uint8_t)bang_bang_flag;
//		
//		int32 temp=0;
//		
//		temp=(int32)L_speed;
//		sd_save[sd_num++]=(uint8_t)(temp);//为上位机读取时兼容以前的图像，这里速度又存了一次
//		sd_save[sd_num++]=(uint8_t)(temp>>8);
//		sd_save[sd_num++]=(uint8_t)(temp>>16);
//		sd_save[sd_num++]=(uint8_t)(temp>>24);
//		
//		temp=(int32)R_speed;
//		sd_save[sd_num++]=(uint8_t)(temp);
//		sd_save[sd_num++]=(uint8_t)(temp>>8);
//		sd_save[sd_num++]=(uint8_t)(temp>>16);
//		sd_save[sd_num++]=(uint8_t)(temp>>24);
//		
//		temp=(int32)L_out;
//		sd_save[sd_num++]=(uint8_t)(temp);
//		sd_save[sd_num++]=(uint8_t)(temp>>8);
//		sd_save[sd_num++]=(uint8_t)(temp>>16);
//		sd_save[sd_num++]=(uint8_t)(temp>>24);
//		
//		temp=(int32)R_out;
//		sd_save[sd_num++]=(uint8_t)(temp);
//		sd_save[sd_num++]=(uint8_t)(temp>>8);
//		sd_save[sd_num++]=(uint8_t)(temp>>16);
//		sd_save[sd_num++]=(uint8_t)(temp>>24);
//		
//		sd_save[sd_num++]=(uint8_t)(cross_out_flag);
//		
//		sd_save[sd_num++]=(uint8_t)circle_first_X;
//		sd_save[sd_num++]=(uint8_t)circle_first_Y;
//		
//		sd_save[sd_num++]=(uint8_t)Prospect_line;
//		
//		sd_save[sd_num++]=(uint8_t)TFmini_data[TFmini_num];
//		sd_save[sd_num++]=(uint8_t)TFmini_useful_flag;
//		
//		trans.t=System_Attitude.Yaw;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		sd_save[sd_num++]=(uint8_t)starting_line_flag;
//		
//		sd_save[sd_num++]=(uint8_t)serve_choose_flag;
//		
//		sd_save[sd_num++]=(uint8_t)cross_out_y;
//		
//	    trans.t=accelerate_k_r;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=accelerate_k_l;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		
//		trans.t=speed_final;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
//		
//		trans.t=System_Attitude.Pitch;;
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[0];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[1];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[2];
//		sd_save[sd_num++]=(uint8_t)trans.byte_t[3];
		
		//DisableInterrupts;
		rc=f_write(&fil_sd,sd_save,2048,&bw);//将图像和其他数据分成两次存，因为f_write必须给定写入的字节数，在image地址之后是随机的数据
		f_sync(&fil_sd);
		//EnableInterrupts;
		write_num++;

		
		if(rc)
		{//写入不成功则关闭文件并重置文件对象的指针
			f_close(&fil_sd);
			fil_sd.fs=0;
		}
	}
}

void fatfs_SD_over(void)
	
{
}
