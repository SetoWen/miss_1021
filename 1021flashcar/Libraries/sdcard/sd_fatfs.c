#include "headfile.h"
#include "ips.h"
#include "picture.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "bsp_sd_fatfs_test.h"
#include "ff.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "picture_crossroad.h"
#include "picture_circle.h"

FIL file_object; //定义文件描述符，
DIR dir_object; //目录对象结构体
FILINFO file_info; //文件信息描述结构体
FRESULT rc;//定义一个函数类型返回值
UINT bw;//写入的字节数
uint16 write_num=0;//记录写入的帧数,每个text文件最多写入13600帧
uint8 sd_save[2048];
#define BUFFER_SIZE (100U)

SDK_ALIGN(uint8_t g_bufferWrite[SDK_SIZEALIGN(BUFFER_SIZE, SDMMC_DATA_BUFFER_ALIGN_CACHE)],
          MAX(SDMMC_DATA_BUFFER_ALIGN_CACHE, SDMMCHOST_DMA_BUFFER_ADDR_ALIGN));
SDK_ALIGN(uint8_t g_bufferRead[SDK_SIZEALIGN(BUFFER_SIZE, SDMMC_DATA_BUFFER_ALIGN_CACHE)],
          MAX(SDMMC_DATA_BUFFER_ALIGN_CACHE, SDMMCHOST_DMA_BUFFER_ADDR_ALIGN));

/*文件系统描述结构体*/
FATFS g_fileSystem; /* File system object */

void SD_fatfs_Init(void)
{
	uint8_t str[20];
	SDCard_Init();
	/*挂载文件系统*/
  f_mount_test(&g_fileSystem);
		sprintf(str,"秃头小姐姐1.txt",0);  //在这里修改写入的文件，同名覆盖，不同名创建新的文件
	rc=f_open(&file_object,str,FA_CREATE_ALWAYS | FA_WRITE);//初始化sd卡
//	rc=fatfs_file_select();
	if(rc) 
	{	
		    if (rc == FR_EXIST)
    {
      PRINTF("文件打开失败.\r\n");
    }
	}
	else PRINTF("\r\n文件打开成功......  \r\n");

}

void SD_test(void)
{
	  /*在SD卡根目录创建一个目录*/
  f_mkdir_test("/dir_1");
 /*创建“/dir_1/f_1.txt”*/
  f_touch_test("/dir_1/秃头小姐.TXT"); 
	  /*打开文件*/
  f_open_test("/dir_1/秃头小姐姐.TXT",&file_object);
//  
//   /*关闭文件*/
  f_close_test(&file_object);
	
	 /*初始化数据缓冲区，为文件的读写做准备*/
  memset(g_bufferWrite, 'a', sizeof(g_bufferWrite));
  g_bufferWrite[BUFFER_SIZE - 2U] = '\r';
  g_bufferWrite[BUFFER_SIZE - 1U] = '\n';
  
  PRINTF("\r\n开始文件读写测试......  \r\n");
  
  f_write_read_test("/dir_1/he.txt", g_bufferWrite, g_bufferRead);  
}

void SD_ImageWrite(void)
{
	uint8_t dat=0;
	uint16_t sd_num=0;
	PRINTF("\r\n开始写入图像......  \r\n");
	if(file_object.obj.fs!=0)//如果文件对象指针指向某个文件，则开始写
	{	
		
		for(int i=0;i<70;i++)
		{
			for(int j=0;j<144;j=j+8)
			{
				dat=0;

				if(image_IterativeThSegment[i][j+0]>1)dat|=0x01;
				if(image_IterativeThSegment[i][j+1]>1)dat|=0x02;
				if(image_IterativeThSegment[i][j+2]>1)dat|=0x04;
				if(image_IterativeThSegment[i][j+3]>1)dat|=0x08;
				if(image_IterativeThSegment[i][j+4]>1)dat|=0x10;
				if(image_IterativeThSegment[i][j+5]>1)dat|=0x20;
				if(image_IterativeThSegment[i][j+6]>1)dat|=0x40;
				if(image_IterativeThSegment[i][j+7]>1)dat|=0x80;
				
				sd_save[sd_num++]=dat;
			}
		}
		for(int i=0;i<70;i++)
		{
			sd_save[sd_num++]=Left_Black_boundary[i];
		}
		for(int i=0;i<70;i++)
		{
			sd_save[sd_num++]=Right_Black_boundary[i];
		}
		for(int i=0;i<70;i++)
		{
			sd_save[sd_num++]=Record_middleline[i];
		}
		sd_save[sd_num++]=(uint8)Right_angle_pointY_down;
		sd_save[sd_num++]=(uint8)Right_angle_pointX_down;
		sd_save[sd_num++]=(uint8)0;
		sd_save[sd_num++]=(uint8)0;
		sd_save[sd_num++]=(uint8)Right_angle_pointY_up;
		sd_save[sd_num++]=(uint8)Right_angle_pointX_up;
		sd_save[sd_num++]=(uint8)Left_angle_pointY_down;
		sd_save[sd_num++]=(uint8)Left_angle_pointX_down;
		sd_save[sd_num++]=(uint8)0;
		sd_save[sd_num++]=(uint8)0;
		sd_save[sd_num++]=(uint8)Left_angle_pointY_up;
		sd_save[sd_num++]=(uint8)Left_angle_pointX_up;
		//70*3+1260=1470
		for(int i=0;i<6;i++)
		{
			sd_save[sd_num++]=Lcircle_pointy[i];
		}
		for(int i=0;i<6;i++)
		{
			sd_save[sd_num++]=Lcircle_pointx[i];
		}
		sd_save[sd_num++]=cross_flag;
		sd_save[sd_num++]=cross_process;
		sd_save[sd_num++]=circle_flag;
		sd_save[sd_num++]=circle_process;
				//DisableInterrupts;
		rc=f_write(&file_object,sd_save,2048,&bw);//将图像和其他数据分成两次存，因为f_write必须给定写入的字节数，在image地址之后是随机的数据
		f_sync(&file_object);
		//EnableInterrupts;
		write_num++;

		
		if(rc)
		{//写入不成功则关闭文件并重置文件对象的指针
			PRINTF("\r\n写入图像失败... \r\n");
			f_close(&file_object);
			file_object.obj.fs=0;
		}
	}
}