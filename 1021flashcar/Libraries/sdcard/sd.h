#ifndef _SD_H
#define _SD_H

#include "headfile.h"

void sd_control(void);
uint8_t fatfs_file_select(void);
uint8_t fatfs_SD_init(void);
extern uint8_t sdcard_flag;
#endif
