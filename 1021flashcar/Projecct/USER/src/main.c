/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2019,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		IAR 8.3 or MDK 5.26
 * @Target core		NXP RT1021DAG5A
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2019-02-18
 ********************************************************************************************************************/


//整套推荐IO查看Projecct文件夹下的TXT文本

//打开新的工程或者工程移动了位置务必执行以下操作
//第一步 关闭上面所有打开的文件
//第二步 project  clean  等待下方进度条走完



#include "headfile.h"
#include "ips.h"
#include "picture.h"
#include "board.h"
#include "bsp_sd_fatfs_test.h"
#include "ff.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "sd_fatfs.h"
#include "Key.h"
#include "global.h"
#include "encoder.h"
     
uint8 test_write[256];
uint8 test_read[256];
int main(void)
{
	uint8 dat;
    DisableGlobalIRQ();
//    board_init();   //务必保留，本函数用于初始化MPU 时钟 调试串口
//  icm20602_init_spi();
//  w25qxx_init();
//  test_write[0] = 15;
//  test_write[4] = 15;
//  w25qxx_erase_sector(0);
//  w25qxx_write_page(0,0,test_write,256);
//  w25qxx_read_page(0,0,test_read,256);  
    /* 初始化开发板时钟 */
    BOARD_BootClockRUN();
    /* 初始化调试串口 */
    BOARD_InitDebugConsole();
	    /* 打印系统时钟 */ 
	  camera_init();
		pit_init();
		EnableGlobalIRQ(0);
    Lcd_Init();	
//		pit_start(PIT_CH0);
		pit_interrupt_ms(PIT_CH1, 5);
		Get_RoadWidth();  //初始化赛道宽度，纯计算,若移动了摄像头的位置或角度要记得更改路宽
		uart_init(USART_1,115200,UART1_TX_B6,UART1_RX_B7);
		uart_init(USART_6,460800,UART6_TX_D20,UART6_RX_D21);    
//		SD_fatfs_Init();
		gpio_init(D0, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
		gpio_init(D1, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
		gpio_init(D2, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);		
		Key_Init();
		Motor_init(); 
//		OtsuThreshold();  //大津法二值化
		gpio_init(D3, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
    while(1)
    { 

       
			if(Mode==DebugMode)
			{
				Debug_Mode();
			}
			if(Mode==RunMode)
			{
				Run_Mode();
			}
    }
		
}
void HardFault_Handler()
{
	LCD_ShowString(0,0,"HardFault_Handler!");
	gpio_set(D0,0);
	gpio_set(D1,0);
	gpio_set(D2,0);
	while(1)
	{
		
	}
}
void MemManage_Handler()
{
	LCD_ShowString(0,0,"MemManage_Handler!");
	gpio_set(D0,0);
	gpio_set(D1,1);
	gpio_set(D2,0);
	while(1)
	{
		
	}
}
void BusFault_Handler()
{
	LCD_ShowString(0,0,"BusFault_Handler!");
	gpio_set(D0,0);
	gpio_set(D1,0);
	gpio_set(D2,1);
	while(1)
	{
		
	}
}
void UsageFault_Handler()
{
	LCD_ShowString(0,0,"UsageFault_Handler!");
	gpio_set(D0,1);
	gpio_set(D1,0);
	gpio_set(D2,0);
	while(1)
	{
		
	}
}

