#include "ips.h"
#include "oledfont.h"
#include "fsl_common.h"
#include "headfile.h"
#include "picture.h"
#include "global.h"
#include "Key.h"

uint8 debug_gray_flag=0; //按键用来切换模式(学长写在key.c)
uint16_t dis_image[ROW][COL];//u16的图像只用于ips显示，存图像要u8 image[][]
uint16_t BACK_COLOR=WHITE, POINT_COLOR=BLUE; //背景色和画笔色
#define USE_IPS
#ifdef USE_IPS
void LPSPI_MasterTransferOnlyWrite(LPSPI_Type *base,uint8_t data)
{
  LPSPI_FlushFifo(base, true, true);//Flushes the LPSPI FIFOs
  LPSPI_ClearStatusFlags(base, kLPSPI_AllStatusFlag);
  LPSPI_WriteData(base, data);
  /* After write all the data in TX FIFO , should write the TCR_CONTC to 0 to de-assert the PCS. Note that TCR
   * register also use the TX FIFO.
   */
  //16为fifo的大小
  while ((LPSPI_GetTxFifoCount(base) == 16))
  {
  }
  base->TCR = (base->TCR & ~(LPSPI_TCR_CONTC_MASK));
}
void LCD_Writ_Bus(uint8_t dat)   //串行数据写入
{ 
  LPSPI_MasterTransferOnlyWrite(LPSPI1,dat);
}
//void LCD_Writ_Bus(uint8_t dat)   //串行数据写入
//{	
//		uint8_t masterRxData = 0;
//    lpspi_transfer_t spi_tranxfer;
//    spi_tranxfer.txData = &dat;   //发送数组
//		spi_tranxfer.rxData = &masterRxData;   //接收数组
//    spi_tranxfer.dataSize = 1;             //长度
//		spi_tranxfer.configFlags = kLPSPI_MasterPcs0|kLPSPI_MasterPcsContinuous;     //片选用PCS0 
//    LPSPI_MasterTransferBlocking(LPSPI1, &spi_tranxfer);         //spi堵塞传输
//}

void LCD_WR_DATA8(uint8_t da) //发送数据-8位参数
{
    IPS_DC_H();//写数据置1，写命令置0
	LCD_Writ_Bus(da); 
}  
 void LCD_WR_DATA(int da)//发送16位
{
    IPS_DC_H();
	LCD_Writ_Bus(da>>8);
    LCD_Writ_Bus(da);
}	  
void LCD_WR_REG(uint8_t da)	 
{
    IPS_DC_L();
	LCD_Writ_Bus(da);
}
 void LCD_WR_REG_DATA(int reg,int da)
{	
    LCD_WR_REG(reg);
	LCD_WR_DATA(da);
}

void Address_set(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2)
{ 
   LCD_WR_REG(0x2a);
   LCD_WR_DATA8(x1>>8);
   LCD_WR_DATA8(x1);
   LCD_WR_DATA8(x2>>8);
   LCD_WR_DATA8(x2);
  
   LCD_WR_REG(0x2b);
   LCD_WR_DATA8(y1>>8);
   LCD_WR_DATA8(y1);
   LCD_WR_DATA8(y2>>8);
   LCD_WR_DATA8(y2);

   LCD_WR_REG(0x2C);					 						 
}

//清屏函数
//Color:要清屏的填充色
void LCD_Clear(uint16_t Color)
{
	uint16_t i,j;  	
	Address_set(0,0,LCD_W-1,LCD_H-1);
    for(i=0;i<LCD_W;i++)
	 {
	  for (j=0;j<LCD_H;j++)
	   	{
        	LCD_WR_DATA(Color);	 			 
	    }
	 }
}
#define Black 0
#define White 255
AT_NONCACHEABLE_SECTION_ALIGN(uint16_t image_binaryzation[120][188], 64); //定义摄像头数据缓存区
AT_NONCACHEABLE_SECTION_ALIGN(uint16_t srcData[120][188], 64); //定义摄像头数据缓存区
AT_NONCACHEABLE_SECTION_ALIGN(uint16_t temp[120][188], 64);


void IPS_DisplayGrayImage(void)
{
	uint16_t color=0;
	uint8_t temp;
//	Image_Sobel();
//	IterativeThSegment();
//	for(int i=0;i<120;i++)
//  {
//		  Address_set(0,i,239,i);		//坐标设置
//      for(int j=0;j<188;j++)
//      {	
//				color=(((*((uint8_t *)csiFrameBuf[0] +  i * APP_CAMERA_WIDTH * 2 + j))>>3))<<11;
//                color=color|((((*((uint8_t *)csiFrameBuf[0] +  i * APP_CAMERA_WIDTH * 2 + j))>>2))<<5);
//                color=color|(((*((uint8_t *)csiFrameBuf[0] +  i * APP_CAMERA_WIDTH * 2 + j))>>3));
//				temp=image_binaryzation[i][j];
//					temp =((uint8_t)(*(csiFrameBuf[0]+i*APP_CAMERA_HEIGHT/153*APP_CAMERA_WIDTH+APP_CAMERA_WIDTH+j*APP_CAMERA_WIDTH/240))>>8);
//					color=(0x001f&((temp)>>3))<<11;
//					color=color|(((0x003f)&((temp)>>2))<<5);
//					color=color|(0x001f&((temp)>>3));
//					LCD_WR_DATA(color);
//      }
//  }  
}
/*LPSPI 主模式*/
/* Select USB1 PLL PFD0 (720 MHz) as lpspi clock source */
#define MASTER_LPSPI_CLOCK_SELECT (1U)
/* Clock divider for lpspi clock source */
#define MASTER_LPSPI_CLOCK_DIVIDER (5U)
#define MASTER_LPSPI_CLOCK_FREQUENCY (CLOCK_GetFreq(kCLOCK_Usb1PllPfd0Clk) / (MASTER_LPSPI_CLOCK_DIVIDER + 1U))
#define MASTER_LPSPI_PCS_FOR_INIT (kLPSPI_Pcs0)
void Lcd_Init(void)
{

    spi_init(IPS_SPIN, IPS_SCL, IPS_SDA, IPS_SDA_IN, IPS_CS, 3, 96*1000000);//硬件SPI初始化
    
    gpio_init(IPS_BL_PIN,GPO,1,GPIO_PIN_CONFIG);
    gpio_init(IPS_DC_PIN,GPO,1,GPIO_PIN_CONFIG);
    gpio_init(IPS_REST_PIN,GPO,0,GPIO_PIN_CONFIG);

	IPS_RES_L();
	systick_delay_ms(10);
	IPS_RES_H();
	systick_delay_ms(10);
		//************* Start Initial Sequence **********// 
	LCD_WR_REG(0x36); //查看36寄存器
	LCD_WR_DATA8(0x00);//显示模式是由8位数据控制，0x02即 0000 0010，第七位即最高位控制上下方向，第六位控制VCC那边还是BLK那边是列起点，第五位控制x轴y轴对调

	LCD_WR_REG(0x3A); 
	LCD_WR_DATA8(0x05);

	LCD_WR_REG(0xB2);
	LCD_WR_DATA8(0x0C);
	LCD_WR_DATA8(0x0C);
	LCD_WR_DATA8(0x00);
	LCD_WR_DATA8(0x33);
	LCD_WR_DATA8(0x33);

	LCD_WR_REG(0xB7); 
	LCD_WR_DATA8(0x35);  

	LCD_WR_REG(0xBB);
	LCD_WR_DATA8(0x19);

	LCD_WR_REG(0xC0);
	LCD_WR_DATA8(0x2C);

	LCD_WR_REG(0xC2);
	LCD_WR_DATA8(0x01);

	LCD_WR_REG(0xC3);
	LCD_WR_DATA8(0x12);   

	LCD_WR_REG(0xC4);
	LCD_WR_DATA8(0x20);  

	LCD_WR_REG(0xC6); 
	LCD_WR_DATA8(0x0F);    

	LCD_WR_REG(0xD0); 
	LCD_WR_DATA8(0xA4);
	LCD_WR_DATA8(0xA1);

	LCD_WR_REG(0xE0);
	LCD_WR_DATA8(0xD0);
	LCD_WR_DATA8(0x04);
	LCD_WR_DATA8(0x0D);
	LCD_WR_DATA8(0x11);
	LCD_WR_DATA8(0x13);
	LCD_WR_DATA8(0x2B);
	LCD_WR_DATA8(0x3F);
	LCD_WR_DATA8(0x54);
	LCD_WR_DATA8(0x4C);
	LCD_WR_DATA8(0x18);
	LCD_WR_DATA8(0x0D);
	LCD_WR_DATA8(0x0B);
	LCD_WR_DATA8(0x1F);
	LCD_WR_DATA8(0x23);

	LCD_WR_REG(0xE1);
	LCD_WR_DATA8(0xD0);
	LCD_WR_DATA8(0x04);
	LCD_WR_DATA8(0x0C);
	LCD_WR_DATA8(0x11);
	LCD_WR_DATA8(0x13);
	LCD_WR_DATA8(0x2C);
	LCD_WR_DATA8(0x3F);
	LCD_WR_DATA8(0x44);
	LCD_WR_DATA8(0x51);
	LCD_WR_DATA8(0x2F);
	LCD_WR_DATA8(0x1F);
	LCD_WR_DATA8(0x1F);
	LCD_WR_DATA8(0x20);
	LCD_WR_DATA8(0x23);

	LCD_WR_REG(0x21); 

	LCD_WR_REG(0x11); 

	LCD_WR_REG(0x29); 

	LCD_Clear(WHITE);
	
} 

//画点
//POINT_COLOR:此点的颜色
void LCD_DrawPoint(uint16_t x,uint16_t y)
{
	Address_set(x,y,x,y);//设置光标位置 
	LCD_WR_DATA(POINT_COLOR); 	    
} 	 
//画一个大点
//POINT_COLOR:此点的颜色
void LCD_DrawPoint_big(uint16_t x,uint16_t y)
{
	LCD_Fill(x-1,y-1,x+1,y+1,POINT_COLOR);
} 
//在指定区域内填充指定颜色
//区域大小:
//  (xend-xsta)*(yend-ysta)
void LCD_Fill(uint16_t xsta,uint16_t ysta,uint16_t xend,uint16_t yend,uint16_t color)
{          
	uint16_t i,j; 
	Address_set(xsta,ysta,xend,yend);      //设置光标位置 
	for(i=ysta;i<=yend;i++)
	{													   	 	
		for(j=xsta;j<=xend;j++)LCD_WR_DATA(color);//设置光标位置 	    
	} 					  	    
}  
//在指定区域内填充指定颜色
//区域大小:
//  (xend-xsta)*(yend-ysta)
void LCD_CursorFill(uint16_t col,uint16_t row,uint16_t width,uint16_t high,uint16_t color)
{          
	uint16_t i,j; 
	uint16_t xsta, ysta, xend, yend;
	xsta=80*(col-1);
	ysta=121+16*(row-1);
	xend=xsta+width;
	yend=ysta+high;
	Address_set(xsta,ysta,xend,yend);      //设置光标位置 
	for(i=ysta;i<=yend;i++)
	{													   	 	
		for(j=xsta;j<=xend;j++)LCD_WR_DATA(color);//设置光标位置 	    
	} 					  	    
}

//画线
//x1,y1:起点坐标
//x2,y2:终点坐标  
void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	uint16_t t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance; 
	int incx,incy,uRow,uCol; 

	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1; 
	uRow=x1; 
	uCol=y1; 
	if(delta_x>0)incx=1; //设置单步方向 
	else if(delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;} 
	if(delta_y>0)incy=1; 
	else if(delta_y==0)incy=0;//水平线 
	else{incy=-1;delta_y=-delta_y;} 
	if( delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y; 
	for(t=0;t<=distance+1;t++ )//画线输出 
	{  
		LCD_DrawPoint(uRow,uCol);//画点 
		xerr+=delta_x ; 
		yerr+=delta_y ; 
		if(xerr>distance) 
		{ 
			xerr-=distance; 
			uRow+=incx; 
		} 
		if(yerr>distance) 
		{ 
			yerr-=distance; 
			uCol+=incy; 
		} 
	}  
}    
//画矩形
void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	LCD_DrawLine(x1,y1,x2,y1);
	LCD_DrawLine(x1,y1,x1,y2);
	LCD_DrawLine(x1,y2,x2,y2);
	LCD_DrawLine(x2,y1,x2,y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void Draw_Circle(uint16_t x0,uint16_t y0,uint8_t r)
{
	int a,b;
	int di;
	a=0;b=r;	  
	di=3-(r<<1);             //判断下个点位置的标志
	while(a<=b)
	{
		LCD_DrawPoint(x0-b,y0-a);             //3           
		LCD_DrawPoint(x0+b,y0-a);             //0           
		LCD_DrawPoint(x0-a,y0+b);             //1       
		LCD_DrawPoint(x0-b,y0-a);             //7           
		LCD_DrawPoint(x0-a,y0-b);             //2             
		LCD_DrawPoint(x0+b,y0+a);             //4               
		LCD_DrawPoint(x0+a,y0-b);             //5
		LCD_DrawPoint(x0+a,y0+b);             //6 
		LCD_DrawPoint(x0-b,y0+a);             
		a++;
		//使用Bresenham算法画圆     
		if(di<0)di +=4*a+6;	  
		else
		{
			di+=10+4*(a-b);   
			b--;
		} 
		LCD_DrawPoint(x0+a,y0+b);
	}
} 
//在指定位置显示一个字符

//num:要显示的字符:" "--->"~"
//mode:叠加方式(1)还是非叠加方式(0)
//在指定位置显示一个字符

//num:要显示的字符:" "--->"~"

//mode:叠加方式(1)还是非叠加方式(0)
void LCD_Showu8(uint16_t x,uint16_t y,uint8_t num,uint8_t mode)
{
    uint8_t temp;
    uint8_t pos,t;
	uint16_t x0=x;
	uint16_t colortemp=POINT_COLOR;      
    if(x>LCD_W-16||y>LCD_H-16)return;	    
	//设置窗口		   
	num=num-' ';//得到偏移后的值
	Address_set(x,y,x+8-1,y+16-1);      //设置光标位置 
	if(!mode) //非叠加方式
	{
		for(pos=0;pos<16;pos++)
		{ 
			temp=asc2_1608[(uint16_t)num*16+pos];		 //调用1608字体
			for(t=0;t<8;t++)
		    {                 
		        if(temp&0x01)POINT_COLOR=colortemp;
				else POINT_COLOR=BACK_COLOR;
				LCD_WR_DATA(POINT_COLOR);	
				temp>>=1; 
				x++;
		    }
			x=x0;
			y++;
		}	
	}else//叠加方式
	{
		for(pos=0;pos<16;pos++)
		{
		    temp=asc2_1608[(uint16_t)num*16+pos];		 //调用1608字体
			for(t=0;t<8;t++)
		    {                 
		        if(temp&0x01)LCD_DrawPoint(x+t,y+pos);//画一个点     
		        temp>>=1; 
		    }
		}
	}
	POINT_COLOR=colortemp;	    	   	 	  
}   
//m^n函数
uint32_t mypow(uint8_t m,uint8_t n)
{
	uint32_t result=1;	 
	while(n--)result*=m;    
	return result;
}			 

//显示2个数字
//x,y:起点坐标
//num:数值(0~99);	 
void LCD_ShowNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len)
{         	
	uint8_t t,temp;						   
	for(t=0;t<len;t++)
	{
		temp=(num/mypow(10,len-t-1))%10;
	 	LCD_Showu8(x+8*t,y,temp+'0',0); 
	}
} 
//显示字符串
//x,y:起点坐标  
//*p:字符串起始地址
//用16字体
void LCD_ShowString(uint16_t x,uint16_t y,const uint8_t *p)
{         

    while(*p!='\0')
    {       
        if(x>LCD_W-16){x=0;y+=16;}
        if(y>LCD_H-16){y=x=0;LCD_Clear(RED);}
        LCD_Showu8(x,y,*p,0);
        x+=8;
        p++;
    }  
}
void LCD_WriteString(uint16_t col,uint16_t row,const uint8_t *p)  //最多3列7行
{         
	////注意该函数最多2列，最多7行
		uint16_t x,y;
		x=80*(col-1)+2;
		y=121+16*(row-1);
    while(*p!='\0')
    {       
        if(x>LCD_W-16){x=0;y+=16;}
        if(y>LCD_H-16){y=x=0;LCD_Clear(RED);}
        LCD_Showu8(x,y,*p,0);
        x+=8;
        p++;
    }  
}
#else
void OLED_Init(void);
#endif

void LCD_Displayimage032_gray(uint16_t *p) 
{
    int i,j; 
	
    uint16 color = 0;
		uint16 temp = 0;

  for(i=0;i<120;i++)
  {
		  Address_set(0,i,239,i);		//坐标设置
      for(j=0;j<240;j++)
      {	
					temp = *(p + i*ROW/120*COL + (COL)+j*(COL)/(240));//读取像素点
					#ifndef Gray_Display
					if(temp==0)
					{
						color=(0x001f&((0)>>3))<<11;
						color=color|(((0x003f)&((0)>>2))<<5);
						color=color|(0x001f&((0)>>3));
					}
					else if(temp==255)
					{
						color=(0x001f&((180)>>3))<<11;
						color=color|(((0x003f)&((180)>>2))<<5);
						color=color|(0x001f&((180)>>3));
					}
					else
					{
						if(debug_gray_flag==1)
						{
							color=(0x001f&((temp)>>3))<<11;
							color=color|(((0x003f)&((temp)>>2))<<5);
							color=color|(0x001f&((temp)>>3));
						}
						else
						{
							color=temp;
						}
					}
					#else
						color=(0x001f&((temp)>>3))<<11;
						color=color|(((0x003f)&((temp)>>2))<<5);
						color=color|(0x001f&((temp)>>3));
					#endif
					LCD_WR_DATA(color);
						
      }
  }  
}

void LCD_display(void)
{
	switch(Mode)
	{
		//调车模式
		case 0: LCD_display_Debug(); break;
		case 1:LCD_display_RunMode(); break;
			
	}
}

void LCD_display_Debug(void)  //调车模式
{
	  LCD_ImageDisplay();  //显示图像
	switch(Screen_Num)  //Debug模式下按KEY1可选择页数
	{
		case 0: LCD_display0(); break;
		case 1:	LCD_display1(); break;
//		case 2:	LCD_display1(); break;
//		case 3:	LCD_display1(); break;
	}
}

//把屏幕分成了上半部分为图像，下半部分调参
//其中调参部分分成了三列，每列7行，即每一页显示21个参数，但要注意的是每个位置不能超过10个字母
//示例:sprintf((char *)str,"Mode:(%d,%d,%d)",a,b,c);
//	   LCD_WriteString(2,2,str);     
//		调用这两个函数即可在第二行第二列打印出Mode:a,b,c
//分成行和列的目的是记录下每个参数的位置存放进一个库中，以便于修改
//从图像到底部121+16*7=217也就是一列就显示7个，中线120
void LCD_display0(void)
{
	int a=0,b=0,c=0;
	uint8 str[10];
	POINT_COLOR=BLACK;
	BACK_COLOR=WHITE;
	a=gpio_get(C8);
	b=gpio_get(C9);
	c=gpio_get(C10);
	sprintf((char *)str,"Mode:(%d,%d,%d)",a,b,c);
	LCD_WriteString(1,1,str);
	sprintf((char *)str,"%d",Key_value);
	LCD_WriteString(1,2,str);
	sprintf((char *)str,"cur:(%d,%d,)",Cursor_x1,Cursor_y1);
	LCD_WriteString(1,3,str);	
	sprintf((char *)str,"%d",test);
	LCD_WriteString(1,4,str);
		sprintf((char *)str,"%d",time);
	LCD_WriteString(1,5,str);
	LCD_WriteString(2,7,"Debug_Mode:");
	
//	sprintf((char *)str,"%.3f",average_error[0]);
//	LCD_ShowString(0,136,str);
//		sprintf((char *)str,"%.3f",average_error[1]);
//	LCD_ShowString(0,152,str);
//		sprintf((char *)str,"%.3f",average_error[2]);
//	LCD_ShowString(0,168,str);
//			sprintf((char *)str,"%.3f",error);
//	LCD_ShowString(0,184,str);
////				sprintf((char *)str,"%d",left_circel_flag[1]);
////	LCD_ShowString(90,184,str);
//				sprintf((char *)str,"(%d,%d)",circle_first_X,circle_first_Y);
//	LCD_ShowString(0,200,str);
//				sprintf((char *)str,"%.3f",derror);
//	LCD_ShowString(0,216,str);
//			LCD_ShowString(90,121,"angle:");
//	sprintf((char *)str,"%.3f",angle[0]);
//	LCD_ShowString(90,136,str);
//		sprintf((char *)str,"%.3f",angle[1]);
//	LCD_ShowString(90,152,str);
//		sprintf((char *)str,"%d and %d",circle_X,circle_Y);
//	LCD_ShowString(90,168,str);
//			sprintf((char *)str,"%d and %d",circle_flag,circle_process);
//	LCD_ShowString(90,184,str);
//				sprintf((char *)str,"(%d,%d)",lost_left_first, lost_left_end);
//	LCD_ShowString(90,200,str);
//				sprintf((char *)str,"(%d,%d",Lcircle_pointx[2],Lcircle_pointy[2]);
//	LCD_ShowString(90,216,str);
//	LCD_ShowString(150,121,"a_error:");
//			sprintf((char *)str,"%.3f",angle_error);
//	LCD_ShowString(150,136,str);
//		LCD_ShowNum(150,152,servo_control,3);

}
void LCD_display1(void)
{
	int a=0,b=0,c=0;
	uint8 str[10];
	POINT_COLOR=BLACK;
	BACK_COLOR=WHITE;
		sprintf((char *)str,"Debug_Mode screem1");
	LCD_WriteString(1,1,str);

	LCD_ShowNum(0,152,Key_value,2);
}


void LCD_display2(void)
{
	int a=0,b=0,c=0;
	uint8 str[10];
	POINT_COLOR=BLACK;
	BACK_COLOR=WHITE;
}
void LCD_display3(void)
{
	int a=0,b=0,c=0;
	uint8 str[10];
	POINT_COLOR=BLACK;
	BACK_COLOR=WHITE;
}
void LCD_display_RunMode(void)  //跑动模式
{
	int a=0,b=0,c=0;
	uint8 str[10];
	LCD_ImageDisplay();
	POINT_COLOR=BLACK;
	BACK_COLOR=WHITE;
	LCD_ShowString(0,121,"Run_Mode:");

}	

void LCD_ImageDisplay(void)
{
	int i,j;
	for(i=0;i<ROW;i++)
	{
		for(j=0;j<COL;j++)
		{
			dis_image[i][j]=image_IterativeThSegment[i][j];
		}
	}
//	for( i=1;i<70;i++)
//	{
//		dis_image[i][Right_Black_boundary[i]]=YELLOW;
//		dis_image[i][Left_Black_boundary[i]]=GREEN;
//		dis_image[i][Record_middleline[i]]=RED;
//	}
//	for( i=lost_left_end;i<lost_left_first;i++)
//	{
//		dis_image[i][1]=BLUE;
//	}
//	for( i=lost_right_end;i<lost_right_first;i++)
//	{
//		dis_image[i][142]=BLUE;
//	}
//	
//	for( i=0;i<140;i++)
//	{
//		dis_image[useful_line][i]=GREEN;
//	}
//	for( i=0;i<140;i++)
//	{
//		dis_image[31][i]=BRRED;
//	}
//	for( i=0;i<140;i++)
//	{
//		dis_image[51][i]=BRRED;
//	}
//	for( i=0;i<140;i++)
//	{
//		dis_image[11][i]=BRRED;
//	}
//	for(i=0;i<68;i++) dis_image[i][30]=BLUE;
//		for(i=0;i<68;i++) dis_image[i][120]=BLUE;
//	for(i=0;i<68;i++) dis_image[i][72]=BLUE;

//	

//	dis_image[Left_angle_pointY_up][Left_angle_pointX_up]=WHITE;
//		dis_image[Right_angle_pointY_up][Right_angle_pointX_up]=WHITE;
		LCD_Displayimage032_gray(dis_image[0]);
}
