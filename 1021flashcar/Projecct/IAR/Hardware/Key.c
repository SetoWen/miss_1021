#include "headfile.h"
#include "global.h"
#include "Key.h"
#include "ips.h"
#include "Position.h"

uint8_t Key_interrupt_flag=0;//发生中断的时候置1，处理完置0
uint8_t Screen_Num=0;
uint8_t Screen_Cursor=152; //光标位置
uint8_t Screen_Cursor_mode=0; //是否进入光标模式
uint8_t Cursor_x1=1,Cursor_y1=1;
uint8_t test=0;


void Key_Init(void)  //开关初始化
{
	
	gpio_interrupt_init(C8,FALLING,GPIO_INT_CONFIG);
	gpio_interrupt_init(C9,FALLING,GPIO_INT_CONFIG);
	gpio_interrupt_init(C10,FALLING,GPIO_INT_CONFIG);
}

void Key_Control_Page(void)  //开关控制页面
{
		if(Key_value==MID)
	 { 
	 	 	Screen_Cursor_mode++;
		 if(Screen_Cursor_mode>2) Screen_Cursor_mode=1;
	 }
	 else if(Key_value==UP)
	 {

		 gpio_toggle(D1);	
	 }
	 else if(Key_value==KEY1)
	 {
		 if(Mode==DebugMode)
		 {
			 Screen_Num++;		
			 Screen_Cursor_mode=0; //换页重新进入光标模式
				LCD_Clear(WHITE);			 
			if(Screen_Num>3) Screen_Num=0;
			
		 }
	 }
	 else if(Key_value==KEY2)
	 {
		 if(Mode==RunMode){return;}
		 if(Mode==DebugMode)
		 {
			 //fatfs_file_select();
			 Mode=RunMode;
		 }
		 LCD_Clear(WHITE); //初始清屏 
		 Screen_Num=0;
		 systick_delay_ms(1000);  //延时2秒 再次期间可以通过按KEY1中断进入调车模式
	 }

 
}

void Key_Control_cursor(void)
{
	uint8_t last_x1=1;
	uint8_t last_y1=1;

	if(Key_value==DOWN)
	 { 
			last_x1=Cursor_x1;
			last_y1=Cursor_y1;		 
	 	 	Cursor_y1+=1;		
		 if(Cursor_y1>7)
		 {
			 if(Cursor_x1==3)
			 {
				 Cursor_x1=1;
				 Cursor_y1=1;
			 }
			 else 
			 {
				 Cursor_x1++;
				 Cursor_y1=1;
			 }	 
		}
			LCD_CursorFill(Cursor_x1,Cursor_y1,Cursor_width,Cursor_high,LIGHTBLUE);
//			LCD_Clear(WHITE);
	 }
	 else if(Key_value==UP)
	 { 
			last_x1=Cursor_x1;
			last_y1=Cursor_y1;		 
	 	 	Cursor_y1-=1;	
		  if(Cursor_y1<1)
		 {
			 if(Cursor_x1==1)
			 {
				 Cursor_x1=3;
				 Cursor_y1=7;
			 }
			 else 
			 {
				 Cursor_x1--;
				 Cursor_y1=7;
			 }	 
		}
			LCD_CursorFill(Cursor_x1,Cursor_y1,Cursor_width,Cursor_high,LIGHTBLUE);
//		 LCD_Clear(WHITE);
	 }
	
	LCD_CursorFill(last_x1,last_y1,Cursor_width,Cursor_high,WHITE); 

}
void Key_ChangeValue(void)
{
	LCD_CursorFill(Cursor_x1,Cursor_y1,Cursor_width,Cursor_high,RED);
	if(Key_value==UP)
	{
		Value_increase(Cursor_x1,Cursor_y1);
	}
	else if(Key_value==DOWN)
	{
		Value_decrease(Cursor_x1,Cursor_y1);
	}
		
}