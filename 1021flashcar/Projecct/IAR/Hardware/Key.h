#ifndef _KEY_H

#define _KEY_H

#include "stdint.h"  //用于uint8_t的定义

#define MID   2  
#define UP    5  
#define DOWN  1
#define RIGHT 6
#define LEFT  4  
#define KEY1  3
#define KEY2  0	
#define Key_value gpio_get(C8)+gpio_get(C9)*2+gpio_get(C10)*4
#define Cursor_width 8
#define Cursor_high 16
#define Last_width  last_x1+8
#define Last_high		last_y1+16

void Key_Init(void);
void Key_Control_cursor(void);
void Key_ChangeValue(void);

extern uint8_t Key_interrupt_flag;
extern uint8_t Screen_Num;
extern uint8_t Screen_Cursor_mode;
extern uint8_t Cursor_x1;
extern uint8_t Cursor_y1;
extern uint8_t test;
#endif