#include "picture.h"
#include "SEEKFREE_MT9V032.h"


extern uint8 image[ROW][COL];
uint8_t image_IterativeThSegment[ROW][COL]; //用来存放迭代法后的图像
int Binaryzation_Value=0;  //迭代法中的阈值
int Gray_Count[256];		//灰度直方图统计

//迭代法采用逼近思想来求阈值
void IterativeThSegment(void)
{
	int i,j;
	int T0=0,T1=0,Z1=0 ,Z2=0,Z1_Count=0,Z2_Count=0; //T0为旧阈值，T1为新阈值,Z1为大于阈值的图层的像素和,Z2为小于阈值的图层的像素和
	for ( i = 0; i < 256; i++)
       Gray_Count[i]=0; //Gray_Count数组用来储存灰度值统计脂直方图

  for ( i = 10; i < 60; i+=2)  //行循环加列循环用来统计灰度直方图  Sample:Gray_Count[2]=5代表原图像中有五个像素值为2的点
    for ( j = 0; j < 144; j+=2)
    {
       Gray_Count[image[i][j]]++;
    }
	T0=40;  //直接设置初始阈值比较简便  //70
  for ( i = 10; i < 50; i+=2)
  {
    for ( j = 0; j < 144; j+=2)
    {
			if(image[i][j]>T0)
			{
				Z1+=image[i][j];  //统计大于阈值的图层的像素和
				Z1_Count++;				//统计大于阈值的图层的像素个数
			}
			else
			{
				Z2+=image[i][j];
				Z2_Count++;
			}
    }
  }
	T1=(Z1/Z1_Count+Z2/Z2_Count)/2;  //求出新阈值T1,公式为T1=(前景平均灰度值+背景平均灰度值)/2
	while(abs(T1-T0)>1 )  //容错范围为正负一
	{
		T0=T1;
		for ( i = 10; i < 60; i+=2)
		{
			for ( j = 0; j < 144; j+=2)
			{
				if(image[i][j]>T0)
				{
					Z1+=image[i][j];
					Z1_Count++;
				}
				else
				{
					Z2+=image[i][j];
					Z2_Count++;
				}
			}
		}
		T1=(Z1/Z1_Count+Z2/Z2_Count)/2;
	}
	if(T1<30)  //阈值太小则定为60
	{
		Binaryzation_Value=30;
	}
	else if(T1>80)  //阈值过大则定为110
	{
		Binaryzation_Value=80;
	}
	else  //阈值处于合适范围内，则认为准确
	{
		Binaryzation_Value=T1;
	}
	
	for ( i = 0; i < 70; i++)  //根据阈值将image二值化为image_IterativeThSegment
	{
		for ( j = 0; j < 144; j++)
		{
			if(image[i][j]>Binaryzation_Value)
			{
				image_IterativeThSegment[i][j]=255;
			}
			else
			{
				image_IterativeThSegment[i][j]=0;
			}
		}
	}
}