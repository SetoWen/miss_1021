#include "headfile.h"
#include "ips.h"
#include "picture.h"
#include "board.h"
#include "bsp_sd_fatfs_test.h"
#include "ff.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "sd_fatfs.h"
#include "Key.h"
#include "global.h"
#include "zf_pit.h"
#include "fsl_common.h"

uint8_t Mode=0;  //车子模式
uint8_t DebugMode=0;
uint8_t RunMode=1;
int time=0;
int time_last=0;

void Debug_Mode(void)   //调车模式
{
  if(mt9v032_finish_flag)
   {		 
		if(Key_interrupt_flag==1)
		{   
			Key_Control_Page();	
			if(Screen_Cursor_mode==1) 
			{
				Key_Control_cursor();
			}
				if(Screen_Cursor_mode==2)
				{
					Key_ChangeValue();
				}
			Key_interrupt_flag=0;
		}
			time_last=pit_get_ms(PIT_CH0);
		 	IterativeThSegment();
			time=pit_get_ms(PIT_CH0)-time_last;

//			pit_close(PIT_CH0);
			LCD_display();
      mt9v032_finish_flag = 0;
//		sendpicture(image);
   }
        
}


void Run_Mode(void)   //跑动模式
{
		if(Key_interrupt_flag==1)
		{   
			Key_Control_Page();	
			Key_interrupt_flag=0;
		}
		 	IterativeThSegment();
			LCD_display();
      mt9v032_finish_flag = 0;
			SD_ImageWrite();
//		sendpicture(image);
 }
 
