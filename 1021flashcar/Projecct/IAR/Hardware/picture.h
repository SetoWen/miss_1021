#ifndef _PICTURE_H

#define _PICTURE_H
#include "SEEKFREE_MT9V032.h"

extern uint8 image_IterativeThSegment[ROW][COL]; //用来存放迭代法后的图像
extern int Binaryzation_Value;  //迭代法中的阈值
extern int Gray_Count[256];		//灰度直方图统计

void IterativeThSegment(void);


#endif