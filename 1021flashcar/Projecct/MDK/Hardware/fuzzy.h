#ifndef _FUZZY_H

#define _FUZZY_H

#include "servo.h"
///////////////////////////////////////////////////
//定义偏差论域
#define   Pos_NB   0   //负大
#define   Pos_NM   1   //负中
#define   Pos_NS   2   //负小
#define   Pos_ZO   3   //零
#define   Pos_PS   4   //正小
#define   Pos_PM   5   //正中
#define   Pos_PB   6   //正大
//定义偏差变化率论域
#define   Dpos_NB   0  //  
#define   Dpos_NM   1  //
#define   Dpos_NS   2  //
#define   Dpos_ZO   3  //
#define   Dpos_PS   4  //
#define   Dpos_PM   5  //
#define   Dpos_PB   6  //

///////////POS///////////
#define   Pos_NB_C   0.0f
#define   Pos_NB_R   1.0f

#define   Pos_NM_L   0.0f
#define   Pos_NM_C   1.0f
#define   Pos_NM_R   2.0f

#define   Pos_NS_L   1.0f
#define   Pos_NS_C   2.0f
#define   Pos_NS_R   3.0f

#define   Pos_ZO_L   2.0f
#define   Pos_ZO_C   3.0f
#define   Pos_ZO_R   4.0f

#define   Pos_PS_L   3.0f
#define   Pos_PS_C   4.0f
#define   Pos_PS_R   5.0f

#define   Pos_PM_L   4.0f
#define   Pos_PM_C   5.0f
#define   Pos_PM_R   6.0f

#define   Pos_PB_L   5.0f
#define   Pos_PB_C   6.0f
///////////DPOS//////////
#define   Dpos_NB_C   0.0f
#define   Dpos_NB_R   1.0f
                      
#define   Dpos_NM_L   0.0f
#define   Dpos_NM_C   1.0f
#define   Dpos_NM_R   2.0f
                          
#define   Dpos_NS_L   1.0f
#define   Dpos_NS_C   2.0f
#define   Dpos_NS_R   3.0f                                                                 
                      
#define   Dpos_ZO_L   2.0f
#define   Dpos_ZO_C   3.0f
#define   Dpos_ZO_R   4.0f
                      
#define   Dpos_PS_L   3.0f
#define   Dpos_PS_C   4.0f
#define   Dpos_PS_R   5.0f
                      
#define   Dpos_PM_L   4.0f
#define   Dpos_PM_C   5.0f
#define   Dpos_PM_R   6.0f
                      
#define   Dpos_PB_L   5.0f
#define   Dpos_PB_C   6.0f

/////////舵机打脚模糊//////////
#define   MAX_range servo_left-servo_center//500

#define   NBA    -MAX_range     
#define   NBB    -420
#define   NBC    -336
#define   NB     -252//65
#define   NM     -168
#define   NS     -84
#define   ZO     0
#define   PS     84
#define   PM     168
#define   PB     252//65
#define   PBC    336
#define   PBB    420
#define   PBA    MAX_range



/////////////////////////////////////////////////
typedef struct //标识符 
{
		//定义论域 
		float Error;  //偏差论域           
		float DError; //微分论域
	
		int   Error_Rule[2];//定义偏差模糊值
		float Error_U[2];		//定义偏差隶属度
	
		int   DError_Rule[2];//定义偏差变化率模糊值
		float DError_U[2];	 //定义偏差变化率隶属度
	
		int   Out_Rule[4];//模糊推理输出量
		float Out_U[4];		//模糊推理隶属度
		
		float Fout_u;
		float Fout;
	
//		float Speed_out_u;
//		float Speed_out;//最终输出值
}Fuzzy;
	













#endif