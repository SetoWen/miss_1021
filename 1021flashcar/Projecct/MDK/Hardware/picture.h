#ifndef _PICTURE_H

#define _PICTURE_H
#include "SEEKFREE_MT9V032.h"

//定义二值化两点
#define White 255
#define Black 0

typedef enum 
{
    Left,
    Right
}Trend;

extern uint8 image_IterativeThSegment[ROW][COL]; //用来存放迭代法后的图像
extern uint8 middle_line[ROW][COL];
extern uint8 leftroadedge[ROW][COL];
extern float LeastSquare_k[3]; 
extern uint8 Record_middleline[70];
extern float angle[3];
extern uint8 Left_Black_boundary[ROW];
extern uint8 Right_Black_boundary[ROW];
extern int  lost_left_first ;
extern int  lost_right_first ;
extern int  lost_left_end ;
extern int  lost_right_end ;
extern uint8 useful_line;
extern float  LeastSquare_b;
extern int left_circel_flag[5];
extern int crossroad_flag;
extern int boundary_Line_last ;
extern float angle_error;
extern float average_error[3];
extern float k_r1,k_l1;
extern int road_width;
extern const uint8 Left_Line;
extern const uint8 Right_Line;
extern const uint8 Bottom_Line;
extern const uint8 Top_Line;
extern uint8 Road_Width[ROW];
extern uint8 Road_Width_P10[ROW];
extern uint8 Road_Width_N10[ROW];
extern uint8_t Record_LeftLine[ROW];
extern uint8_t Record_RightLine[ROW]; 

void IterativeThSegment(void);
void IterativeThSegment_boundary(void);
void OtsuThreshold(void);
void GetLeftRoadEdge(void);
void Image_Lost(void);
void Useful_Track(void);
void useful_line_assist(void);
void LeastSquare(void);
void circle_check(void);
void Addline_image(void);
void middle_error(void);
void Get_RoadWidth(void);
void LeastSquare_boundary(void);
int Trend_Boundary(uint8* array, Trend trend) ;
void Record_Boundary(void);
uint8 twopoints_Addline(int x1,int x2,int y1,int y2,int x);


#endif