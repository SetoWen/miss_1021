#include "picture.h"
#include "servo.h"
#include "SEEKFREE_MT9V032.h"
#include "ips.h"
#include "zf_pwm.h"

float error_last=0;
float error=0;
float derror=0;
float servo_control=0;
int dangle_error=0;

////////////////////////////////////////
int servo_kp=20;
int servo_kd=40;
float middle_error_0=0.0;
float middle_error_1=0.6;
float middle_error_2=0.4;
///////////////////////////////////////
//typedef struct  
// {
//	 int kp;
//	 int kd;
//	 float middle_error_0;
//	 float middle_error_1;
//	 float middle_error_2;
// }Pid_servo;
// 
// Pid_servo Pid_servo_t={
//	 20,
//	 40,
//	 0.0,
//	 0.6,
//	 0.4
// };

void servo_LeastSquare(void)
{

	/*
	if(angle_error>=0)
	{
		servo_control=servo_center-(servo_center-servo_left)*angle_error/40;
	}
	if(angle_error<-5)
	{
		servo_control=servo_center-(servo_right-servo_center)*angle_error/40;		
	}
	*/
	error=average_error[1]*middle_error_1;+average_error[2]*middle_error_2;
	derror=error-error_last;
	servo_control=servo_center-((servo_kp)*error+servo_kd*derror);
  error_last=error;
	//servo_control=servo_center+servo_kp*angle_error;
	if(servo_control<=servo_right) servo_control=servo_right;
  if(servo_control>=servo_left) servo_control=servo_left;
	pwm_duty(PWM2_MODULE3_CHA_C28, servo_control);
}
