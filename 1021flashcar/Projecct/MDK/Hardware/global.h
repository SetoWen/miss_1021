#ifndef _GLOBAL_H

#define _GLOBAL_H

#include "stdint.h"  //用于uint8_t的定义

void Debug_Mode(void);
void Run_Mode(void);

extern uint8_t Mode;
extern uint8_t DebugMode;
extern uint8_t RunMode;
extern int time;

#endif