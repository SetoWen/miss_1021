#include "Fuzzy.h"
#include "headfile.h"

Fuzzy Fuzzy_Servo;//申请舵机模块控制结构体
float min(float,float);

const int Fuzzy_Servo_Rule[7][7]=
{  
// NB        NM       NS     ZO     PS      PM        PB
  {NBA,     NBB,     NBC,    NB,    NM,     PS,       ZO },  // NB 
  {NBB,     NBC,     NB,     NM,    NS,     PS,       PS },  // NM  
  {NBC,     NB,      NM,     NS,    ZO,     PS,       PM },  // NS 
	
  {NB,      NM,      NS,     ZO,    PS,     PM,       PB },  // ZO  
	
  {NM,      NS,      ZO,     PS,    PM,     PB,       PBC},  // PS 
  {NS,      NS,      PS,     PM,    PB,     PBC,      PBB},  // PM   
  {ZO,      NS,      PM,     PB,    PBC,    PBB,      PBA},  // PB 
};



///////////////////////////////////////////////////////////////////////
//偏差输入量化
float Fuzzy_E_Input(float error)
{
	float K=0.0;
	K=0.0700f;//量化因子
	float temp=0.0;
	
	temp=error*K;
	
	if(temp>3.0)temp=3.0;
	if(temp<-3.0)temp=-3.0;
	return temp+3.0;//将输入值量化成[0,6]之间
}

//偏差变化率输入量化
float Fuzzy_EC_Input(float derror)
{
	float K=0.18;
	float temp=0.0;
	
	temp=derror*K;
	if(temp>3.0)temp=3.0;
	if(temp<-3.0)temp=-3.0;
	return temp+3.0;
}

//////////////////////////////////////////////////////////////////////////
//偏差解模糊、求隶属度
void Error_Fuzzy(Fuzzy* Fz) 
{
    int i=0,j=0;
    float f1,f2;
    for(j=0;j<2;j++)
    {  
      Fz->Error_Rule[j]=0;//先将之前运算的隶属度变为0
    }
		//0-1
    if(Fz->Error < Pos_NB_R)        //论域1    
    {
      Fz->Error_Rule[i] = Pos_NB;  //确定模糊值
      
      f2 = (Pos_NB_R - Fz->Error); 
      f1 = f2;//只有一个隶属度
            
      Fz->Error_U[i] = min(f1,f2);  //计算隶属度

      i++;
    }
		//1-2
    if((Fz->Error>=Pos_NM_L)&&(Fz->Error<Pos_NM_R))//论域2
    { 
      Fz->Error_Rule[i]=Pos_NM;    //确定模糊值
      
      f1 = (Fz->Error - Pos_NM_L);//模糊值减去区间最小值
      f2 = (Pos_NM_R - Fz->Error);//模糊值减去区间最大值
      
      Fz->Error_U[i]=min(f1,f2);  //记录离较近边界的差
      
      i++;
    }
//     
    if((Fz->Error>=Pos_NS_L)&&(Fz->Error<Pos_NS_R))//论域3
    {
      Fz->Error_Rule[i]=Pos_NS;
      
      f1 = (Fz->Error - Pos_NS_L);
      f2 = (Pos_NS_R - Fz->Error);
      
      Fz->Error_U[i]=min(f1,f2);

      i++;
    }
//     
    if((Fz->Error>=Pos_ZO_L)&&(Fz->Error<Pos_ZO_R))//论域4
    {
      Fz->Error_Rule[i]=Pos_ZO ;
      
      f1 = (Fz->Error - Pos_ZO_L);
      f2 = (Pos_ZO_R - Fz->Error);
      
      Fz->Error_U[i]=min(f1,f2);

      i++;
    }
//    
    
    if((Fz->Error>=Pos_PS_L)&&(Fz->Error<Pos_PS_R))//论域5
    {
      Fz->Error_Rule[i]=Pos_PS;
      
      f1 = (Fz->Error - Pos_PS_L);
      f2 = (Pos_PS_R - Fz->Error);
      
      Fz->Error_U[i]=min(f1,f2);

      i++;
    }
//    
    if((Fz->Error>=Pos_PM_L)&&(Fz->Error<Pos_PM_R))//论域6
    {
      Fz->Error_Rule[i]=Pos_PM;
      
      f1 = (Fz->Error - Pos_PM_L);
      f2 = (Pos_PM_R - Fz->Error);
      
      Fz->Error_U[i]=min(f1,f2);

      i++;
    }
//  
    if(Fz->Error >= Pos_PB_L)               //论域7
    {
      Fz->Error_Rule[i]=Pos_PB;
      
      f1 = (Fz->Error - Pos_PB_L);
      f2 = f1;
      
      Fz->Error_U[i]=min(f1,f2);

      i++;
    }
//      
    if(i == 1) 
    {
       Fz->Error_U[1]=0;
    }
}


//偏差变化率解模糊、求隶属度
void DError_Fuzzy(Fuzzy* Fz) 
{
    int i=0,j=0;
    float f1,f2;
    for(j=0;j<2;j++)
    {  
      Fz->DError_Rule[j]=0;//先将之前运算的隶属度变为0
    }

    if(Fz->DError < Pos_NB_R)        //论域1    01之间 模糊值为1 隶属度为
    {
      Fz->DError_Rule[i] = Dpos_NB;  //确定模糊值
      
      f2 = (Dpos_NB_R - Fz->DError);
      f1 = f2;
            
      Fz->DError_U[i] = min(f1,f2);  //计算隶属度

      i++;
    }
  
    if((Fz->DError>=Pos_NM_L)&&(Fz->DError<Pos_NM_R))//论域2
    { 
      Fz->DError_Rule[i]=Pos_NM;
      
      f1 = (Fz->DError - Dpos_NM_L);
      f2 = (Dpos_NM_R - Fz->DError);
      
      Fz->DError_U[i]=min(f1,f2);;
      
      i++;
    }
//     
    if((Fz->DError>=Pos_NS_L)&&(Fz->DError<Pos_NS_R))//论域3
    {
      Fz->DError_Rule[i]=Pos_NS;
      
      f1 = (Fz->DError - Dpos_NS_L);
      f2 = (Dpos_NS_R - Fz->DError);
      
      Fz->DError_U[i]=min(f1,f2);

      i++;
    }
//     
    if((Fz->DError>=Pos_ZO_L)&&(Fz->DError<Pos_ZO_R))//论域4
    {
      Fz->DError_Rule[i]=Pos_ZO ;
      
      f1 = (Fz->DError - Dpos_ZO_L);
      f2 = (Dpos_ZO_R - Fz->DError);
      
      Fz->DError_U[i]=min(f1,f2);

      i++;
    }
//    
    
    if((Fz->DError>=Pos_PS_L)&&(Fz->DError<Pos_PS_R))//论域5
    {
      Fz->DError_Rule[i]=Pos_PS;
      
      f1 = (Fz->DError - Dpos_PS_L);
      f2 = (Dpos_PS_R - Fz->DError);
      
      Fz->DError_U[i]=min(f1,f2);

      i++;
    }
//    
    if((Fz->DError>=Pos_PM_L)&&(Fz->DError<Pos_PM_R))//论域6
    {
      Fz->DError_Rule[i]=Pos_PM;
      
      f1 = (Fz->DError - Dpos_PM_L);
      f2 = (Dpos_PM_R - Fz->DError);
      
      Fz->DError_U[i]=min(f1,f2);

      i++;
    }
//  
    if(Fz->DError >= Pos_PB_L)               //论域7
    {
      Fz->DError_Rule[i]=Pos_PB;
      
      f1 = (Fz->DError - Dpos_PB_L);
      f2 = f1;
      
      Fz->DError_U[i]=min(f1,f2);

      i++;
    }
//      
    if(i == 1) 
    {
       Fz->DError_U[1]=0;
    }
}
//////////////////////////////////////////////////////////////////////////
//根据模糊值和隶属度求需要输出的模糊值和隶属度
float Fuzzy_out(Fuzzy* Fz,float error,float derror) 
{
  uint8 i,j;
  
  Fz->Error=Fuzzy_E_Input(error);  //对检测到的偏差量化
  Fz->DError=Fuzzy_EC_Input(derror); //对计算出的偏差变化率量化
  
  Error_Fuzzy(Fz);//E模糊化，得模糊值与隶属度
  DError_Fuzzy(Fz);//EC模糊化，得模糊值与隶属度
  
	/*根据模糊规则得出相应模糊值*/ 
		Fz->Out_Rule[0] = Fuzzy_Servo_Rule[Fz->DError_Rule[0]][Fz->Error_Rule[0]]; 
		Fz->Out_Rule[1] = Fuzzy_Servo_Rule[Fz->DError_Rule[0]][Fz->Error_Rule[1]];
		Fz->Out_Rule[2] = Fuzzy_Servo_Rule[Fz->DError_Rule[1]][Fz->Error_Rule[0]];
		Fz->Out_Rule[3] = Fuzzy_Servo_Rule[Fz->DError_Rule[1]][Fz->Error_Rule[1]];

  /*根据E和EC的相应隶属度求U得相应隶属度*/
  Fz->Out_U[0] = min(Fz->DError_U[0],Fz->Error_U[0]);
  Fz->Out_U[1] = min(Fz->DError_U[0],Fz->Error_U[1]);
  Fz->Out_U[2] = min(Fz->DError_U[1],Fz->Error_U[0]);
  Fz->Out_U[3] = min(Fz->DError_U[1],Fz->Error_U[1]);

  for(i=0;i<3;i++)
  { 
   for(j=i+1;j<4;j++) 
   {  
     if(Fz->Out_Rule[i] == Fz->Out_Rule[j]) 
     {
        if(Fz->Out_U[i] > Fz->Out_U[j]) 
        {          
          Fz->Out_U[j]=0;
        }
        else 
        {
          Fz->Out_U[i]=0;
        }
     }
    
   }
  }
	
  //解模糊
  Fz->Fout_u=0;
  Fz->Fout=0;
  

 /*加权平均法解模糊*/
  for(i=0;i<4;i++) 
  {
    Fz->Fout   += (float)Fz->Out_Rule[i]*Fz->Out_U[i];
    Fz->Fout_u += (float)(Fz->Out_U[i]); 
  }
  Fz->Fout =Fz->Fout / Fz->Fout_u;
	
	return Fz->Fout;

}
//serve=serve_middle-Fuzzy_out(&Fuzzy_Servo,error,derror);

////////最小值////////////
float min(float x,float y)
{
	if(x>y)
		return y;
	else
		return x;
}