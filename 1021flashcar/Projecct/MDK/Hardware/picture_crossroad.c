#include "picture.h"
#include "ips.h"
#include "picture_circle.h"
#include "picture_crossroad.h"
void out_of_crossroad(void);
int Right_angle_pointY_down = 135;//左下拐点2个(求斜率)，左上一个
int Right_angle_pointX_down = 65;
int Right_angle_pointY_up = 135;
int Right_angle_pointX_up = 4;
int Left_angle_pointY_down = 4;
int Left_angle_pointX_down = 65;
int Left_angle_pointY_up = 4;
int Left_angle_pointX_up = 4;
//入环标志
uint8 cross_flag=0;
uint8 cross_process=0;
//出环
uint8 cross_out_x = 0;
uint8 cross_out_y = 0;
uint8 cross_out_flag = 0;


uint8	Cross_right_down_flag = 0;
uint8	Cross_left_down_flag = 0;
uint8	Cross_right_up_flag = 0;
uint8	Cross_left_up_flag = 0;
void crossroad_check(void)
{
	int temp = 0;
	int i;
	//用于再更新上拐点搜索的起始点
	int temp_left;
	int temp_right;
	const uint8 down_point_judge = 5;//太大斜出十字会太晚补线           
	const uint8 up_point_judge = 15;//太小会误判,最小也要13
	
	Cross_right_down_flag = 0;
	Cross_left_down_flag = 0;
	Cross_right_up_flag = 0;
	Cross_left_up_flag = 0;
	
	//搜左下角点
	for (i = Bottom_Line - 8; i > 15; i--)//原为i = Bottom_Line - 20; i > 15; i--
	{
		if (abs(Left_Black_boundary[i] - Left_Black_boundary[i +1])<5 &&
		   Left_Black_boundary[i] - Left_Black_boundary[i - 3] > down_point_judge &&
		   Left_Black_boundary[i] - Left_Black_boundary[i - 4] > down_point_judge)

		{
			Left_angle_pointY_down = Left_Black_boundary[i];
			Left_angle_pointX_down = i;

			if (Left_angle_pointY_down > COL / 2 + 15)
			{
				Cross_left_down_flag = 0;
				break;
			}

			Cross_left_down_flag = 1;
			break;
		}
		else
		{
			Cross_left_down_flag = 0;

		}
	}

	if (Cross_left_down_flag == 1)
	{
		temp = 0;
		for (i = Left_angle_pointX_down - 5; i < Bottom_Line - 10; i++)
		{
			if (image_IterativeThSegment[i][Left_angle_pointY_down + 5] == Black)
			{
				temp++;
			}
			if (temp > 0)
			{
				Cross_left_down_flag = 0;
				break;
			}
		}
	}
	//搜索右下拐点
	for ( i = Bottom_Line - 8; i > 8; i--)  //原为i = Bottom_Line - 20; i > 15; i--
	{
		if (abs(Right_Black_boundary[i] - Right_Black_boundary[i + 1] )<5&&
			Right_Black_boundary[i - 3] - Right_Black_boundary[i] > down_point_judge &&
			Right_Black_boundary[i - 4] - Right_Black_boundary[i] > down_point_judge)

		{
			Right_angle_pointY_down = Right_Black_boundary[i];
			Right_angle_pointX_down = i;

			if (Right_angle_pointY_down < COL / 2 - 15)
			{
				Cross_right_down_flag = 0;
				break;
			}

			Cross_right_down_flag = 1;
			break;
		}
		else
		{
			Cross_right_down_flag = 0;
		}
	}

	if (Cross_right_down_flag == 1)
	{
		temp = 0;
		for ( i = Right_angle_pointX_down - 5; i < Bottom_Line - 10; i++)
		{
			if (image_IterativeThSegment[i][Right_angle_pointY_down - 5] == Black)
			{
				temp++;
			}
			if (temp > 0)
			{
				Cross_right_down_flag = 0;
				break;
			}
		}
	}

	//过滤掉错误的下拐点，下拐点需要成对出现
	if (Cross_right_down_flag == 0 || Cross_left_down_flag == 0)
	{
		Cross_right_down_flag = 0;
		Cross_left_down_flag = 0;
	}
	do
	{
		if (Cross_right_down_flag == 1)
		{
			temp_right = Right_angle_pointX_down;
		}
		else
		{
			temp_right = Bottom_Line - 20;
		}

		if (Cross_left_down_flag == 1)
		{
			temp_left = Left_angle_pointX_down;
		}
		else
		{
			temp_left = Bottom_Line - 20;
		}
	}
	while (0);

	//搜左上拐点
	for ( i = temp_left; i > 9; i--)
	{
		if (/*Left_Black_boundary[i] <= Left_Black_boundary[i - 4] &&*/
			Left_Black_boundary[i] - Left_Black_boundary[i + 5] >up_point_judge &&
			abs(Left_Black_boundary[i] - Left_Black_boundary[i - 1])<5)
		{
			Left_angle_pointY_up = Left_Black_boundary[i];//记录拐点坐标值
			Left_angle_pointX_up = i;

			if (Left_angle_pointY_up > COL / 2 + 15)
			{
				Cross_left_up_flag = 0;
				break;
			}

			Cross_left_up_flag = 1;
			break;
		}
		else
		{
			Cross_left_up_flag = 0;
		}
	}

	if (Cross_left_up_flag == 1)
	{
		temp = 0;
		for (i= Left_angle_pointX_up - 5; i < Bottom_Line - 10; i++)  //Bottom_Line - 10
		{
			if (image_IterativeThSegment[i][Left_angle_pointY_up + 20] == Black || image_IterativeThSegment[i + 6][Left_angle_pointY_up] == Black)
			{
				temp++;
			}
			if (temp > 0)
			{
				Cross_left_up_flag = 0;
				break;
			}
		}
	}

	//搜右上拐点
	for ( i = temp_right; i > 9; i--)
	{
		if (/*Right_Black_boundary[i - 4] <= Right_Black_boundary[i] &&*/
		   Right_Black_boundary[i + 5] - Right_Black_boundary[i]>up_point_judge&&
		   abs(Right_Black_boundary[i] - Right_Black_boundary[i - 1])<5)
		{
			Right_angle_pointY_up = Right_Black_boundary[i];//记录拐点坐标值
			Right_angle_pointX_up = i;

			if (Right_angle_pointY_up < COL / 2 - 15)
			{
				Cross_right_up_flag = 0;
				break;
			}

			Cross_right_up_flag = 1;
			break;
		}
		else
		{
			Cross_right_up_flag = 0;
		}
	}

	if (Cross_right_up_flag == 1)
	{
		temp = 0;
		for (i = Right_angle_pointX_up - 5; i < Bottom_Line - 10; i++)
		{
			if (image_IterativeThSegment[i][Right_angle_pointY_up - 20] == Black || image_IterativeThSegment[i + 6][Right_angle_pointY_up] == Black)
			{
				temp++;
			}
			if (temp > 0)
			{
				Cross_right_up_flag = 0;
				break;
			}
		}
	}
	//找不到拐点即人工置点
	if (Cross_left_down_flag == 0)
		{
			Left_angle_pointY_down = 4;
			Left_angle_pointX_down = 65;
		}
		else if (Cross_right_down_flag == 0)
		{
			Right_angle_pointY_down = 140;
			Right_angle_pointX_down = 65;
		}
		else if (Cross_left_up_flag == 0)
		{
			Left_angle_pointY_up = 4;
			Left_angle_pointX_up = 4;
		}
		else if (Cross_right_up_flag == 0)
		{
			Right_angle_pointY_up = 140;
			Right_angle_pointX_up = 4;
		}
		/////////////////////////////////
		//判断十字补线类型

		//搜到4个点
		if (Cross_left_down_flag == 1 &&
			Cross_right_down_flag == 1 &&
			Cross_right_up_flag == 1 &&
			Cross_left_up_flag == 1)
		{
			cross_flag = 1;
		}

		//搜到左上，右上，比较容易误判
		else if (Cross_right_up_flag == 1 &&
			Cross_left_up_flag == 1)
		{
			cross_flag = 2;
		}

		//缺左上
		else if (Cross_right_down_flag == 1 &&
			Cross_left_down_flag == 1 &&
			Cross_right_up_flag == 1)
		{
			cross_flag = 3;
		}

		//缺右上
		else if (Cross_right_down_flag == 1 &&
			Cross_left_down_flag == 1 &&
			Cross_left_up_flag == 1)
		{
			cross_flag = 4;
		}
		
		//仅有两个下拐点（极端）
		else if (Cross_right_down_flag == 1 &&
			Cross_left_down_flag == 1)
		{
			cross_flag = 5;
		}

		else
		{
			cross_flag = 0;
		}
		out_of_crossroad();
		//十字类型判断完毕
		//////////////////////////////////////////////////
	}

void out_of_crossroad(void)
{
		int i;
		uint8 temp = 0;
		cross_out_x = 0;
		cross_out_y = 0;
		cross_out_flag = 0;

		/*if(cross_flag!=0||road_lost_flag!=0||circle_flag!=0||Ramp_flag!=0||Block_flag!=0)
		{
			return;
		}*/
		//搜索左下拐点
		for ( i = Bottom_Line - 10; i > 10; i--)
		{
			if ( Left_Black_boundary[i] - Left_Black_boundary[i - 4] > 10 &&
				 image_IterativeThSegment[i - 1][Left_Black_boundary[i]] == White &&
				 image_IterativeThSegment[i - 2][Left_Black_boundary[i]] == White)
			{
				cross_out_y = Left_Black_boundary[i];
				cross_out_x = i;

				if (cross_out_y < COL / 2-33)
				{
					cross_out_flag = 0;
					break;
				}

				cross_out_flag = 1;
				break;
			}
			else
			{
				cross_out_flag = 0;

			}
		}

		if (cross_out_flag == 1)  //判断拐点下的右边界是否几乎全丢，更精确
		{
			temp = 0;
			for ( i = cross_out_x; i < Bottom_Line; i++)
			{
				if (Right_Black_boundary[i] != 142)
				{
					temp++;
				}
				if (temp > 3)
				{
					cross_out_flag = 0;
					break;
				}
			}
		}

		if (cross_out_flag == 1) //判断拐点所在的列是否比下面每一行边界的列要大，确保准确
	    {
			temp = 0;
			for ( i = cross_out_x; i < Bottom_Line - 20; i++)
			{
				if (cross_out_y < Left_Black_boundary[i] || cross_out_y > Right_Black_boundary[i])
				{
					temp++;
				}
				if (temp > 0)
				{
					cross_out_flag = 0;
					break;
				}
			}
		}

		if(cross_out_flag==1)
		{
			if(cross_out_x>40) //拐点不超过40行
			{
				cross_out_flag=0;
			}
		}
		
		if (cross_out_flag == 0)
		{
			cross_out_x = 0;
			cross_out_y = 0;
		}

		if(cross_out_flag==1)
		{
			return;
		}
		//搜索右下拐点
		for ( i = Bottom_Line - 20; i > 15; i--)
		{
			if (Right_Black_boundary[i - 4] - Right_Black_boundary[i] > 10 &&
				image_IterativeThSegment[i - 1][Right_Black_boundary[i]] == White &&
				image_IterativeThSegment[i - 2][Right_Black_boundary[i]] == White)

			{
				cross_out_y = Right_Black_boundary[i];
				cross_out_x = i;

				if (cross_out_y > COL / 2+20)
				{
						cross_out_flag = 0;
						break;
				}

				cross_out_flag = 1;
				break;
			}
			else
			{
				cross_out_flag = 0;
			}
		}

		if (cross_out_flag == 1) //判断拐点下的左边界是否几乎全丢，更精确
		{
			temp = 0;
			for ( i = cross_out_x; i < Bottom_Line; i++)
	    	{
				if (Left_Black_boundary[i] != 1)
				{
					temp++;
				}
				if (temp > 3)
				{
					cross_out_flag = 0;
					break;
				}
			}
		}

		if (cross_out_flag == 1) //判断拐点所在的列是否比下面每一行边界的列要大，确保准确
		{
			temp = 0;
			for ( i = cross_out_y; i < Bottom_Line - 20; i++)
			{
				if (cross_out_y < Left_Black_boundary[i] || cross_out_y > Right_Black_boundary[i])
				{
					temp++;
				}
				if (temp > 0)
				{
					cross_out_flag = 0;
					break;
				}
			}
		}
		
   	    if(cross_out_flag==1)
		{
			if(cross_out_x>40)  //拐点不超过40行
			{
				cross_out_flag=0;
			}
		}
		
		if (cross_out_flag == 0)
		{
				cross_out_x = 0;
				cross_out_y = 0;
		}	
}