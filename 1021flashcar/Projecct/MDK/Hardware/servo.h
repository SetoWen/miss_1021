#ifndef _servo_H

#define _servo_H


#define servo_center 3500
#define servo_left   4000
#define servo_right  3000
#define screen_middle 72

void servo_LeastSquare(void);
void servo_fuzzy(void);
extern float error;
extern float derror;
extern int servo_kp;
extern int servo_kd;

extern float angle_error;
extern float servo_control;
extern float error_last;
#endif