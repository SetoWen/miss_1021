#include "headfile.h"
#include "ips.h"
#include "picture.h"
#include "board.h"
#include "bsp_sd_fatfs_test.h"
#include "ff.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "sd_fatfs.h"
#include "Key.h"
#include "global.h"
#include "zf_pit.h"
#include "servo.h"
#include "encoder.h"
#include "fsl_common.h"
#include "picture_circle.h"
#include "picture_crossroad.h"
#include "nrf.h"


uint8_t Mode=0;  //车子模式
uint8_t DebugMode=0;
uint8_t RunMode=1;
int time=0;
int time_last=0;

void Debug_Mode(void)   //调车模式
{
  if(mt9v032_finish_flag)
   {		 
		if(Key_interrupt_flag==1)
		{   
			Key_Control_Page();	
			if(Screen_Cursor_mode==1) 
			{
				Key_Control_cursor();
			}
				if(Screen_Cursor_mode==2)
				{
					Key_ChangeValue();
				}
			Key_interrupt_flag=0;
		}
//			time_last=pit_get_ms(PIT_CH0);
		 	IterativeThSegment(); //迭代法二值化
//		  OtsuThreshold();  //大津法二值化
			IterativeThSegment_boundary();
			Image_Lost();
			crossroad_check();
			Circle_Search();
			Addline_image();
			middle_error();
//			time=pit_get_ms(PIT_CH0)-time_last;

//			pit_close(PIT_CH0);
			servo_LeastSquare();
			LCD_display();
		  pwm_duty(PWM2_MODULE0_CHB_D7, 0);
		  pwm_duty(PWM2_MODULE0_CHA_D6, 0);
			pwm_duty(PWM2_MODULE1_CHB_D5, 0);
		  pwm_duty(PWM2_MODULE1_CHA_D4, 0);	
      mt9v032_finish_flag = 0;
//			sendpicture(image);
//		 niming_report_SENSER();
   
 }    
}
//除去lcd和sd卡1.2ms
//加上lcd  5.6ms
//纯motorcontrol  9.5us
//sd卡存图像  1.8ms


void Run_Mode(void)   //跑动模式
{
		if(Key_interrupt_flag==1)
		{   
			Key_Control_Page();	
			Key_interrupt_flag=0;
		}
		if(mt9v032_finish_flag)
		{
//			gpio_set(D3,0);
			IterativeThSegment();
			IterativeThSegment_boundary();
			Image_Lost();
			crossroad_check();
//			Circle_Search();
			Addline_image();
			middle_error();			
			servo_LeastSquare();			
//			motor_control();			
////		  LCD_display();			
//			SD_ImageWrite();			
			niming_report_SENSER();
      mt9v032_finish_flag = 0;
//		sendpicture(image);
			}
		
//		pit_get_ms(PIT_CH0);
 }
 
