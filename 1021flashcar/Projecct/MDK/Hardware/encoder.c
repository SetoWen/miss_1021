#include "servo.h"
#include "global.h"
#include "encoder.h"
#include "headfile.h"

//周期0.005s
//轮胎直径0.063m
//编码器512线
//轮子的周长：20.00cm  
//轮子转一圈，脉冲数为1175（待定）    
//编码器转一圈，脉冲数为515 （待定）
//给他pwm=5000，则速度为1.66m/s （待定）
//pwm=10000,速度为1m/s
//5000为系数
//speed=out/2260*0.1884/0.005=1.66m/s
//out=speed*2260*0.005/0.1884
//每一点pwm为0.000332m/s,则2m/s的pwm为6024

int encoder_l,encoder_r,aver_encoder;  
float avert_speed;
int L_SpeedArray[3],R_SpeedArray[3];         //读取编码器的数据
float L_Speed,R_Speed;
float L_pwm,R_pwm;
float L_error,R_error;

 
float perimeter=0.2f;
int init_pwm=10000;
float init_speed=1.0f;
int wheel_pulse=1175;

int L_ki_value=0,R_ki_value=0;
int L_kp_value=0,R_kp_value=0;


//////////////////////////位置式/////////////////////////////////
float speedl_set=2.5,speedr_set=2.5;
float speedl_set1=2.5,speedr_set1=2.5;
float motor_kp=25000.0f;  // 记得乘上init_pwm=10000
float motor_ki=2000.0f;  
float motor_kd=0.0;
float cycle=0.005f;
//////////////////////////增量式////////////////////////////////
int Speed_Base=15000;

float motor_kp_t=0.0f;
float motor_ki_t=0.0f;
float motor_kd_t=0.0f;

float L_error_t[3]={0.0,0.0,0.0},R_error_t[3]={0.0,0.0,0.0};
int L_ki_value_t=0,R_ki_value_t=0;
int L_kp_value_t=0,R_kp_value_t=0;
int L_kd_value_t=0,R_kd_value_t=0;

int L_error_sum,R_error_sum;
int   L_out_t=0,R_out_t=0;
/////////////////////////////////////////////////////////
//左右轮距离为15.5cm
//前后轮距离为21cm
//pwm=角度*10
//左 4000  中 3500  右 3000
float angle_t=0.0f;
float Dif=10.0f;
float Dif_array[3]={0.0,0.0,0.0};
float B_w=15.5;
float L_l=21;
float EDS_Coefficient_t=0.3;                   //B_w/(2*L_l)=0.369

int L_out=0;
int R_out=0;

int encoder_l1,encoder_r1;

//typedef struct  
// {
//	 float kp;
//	 float ki;
//	 float kd;
//	 float period;
//	 
// }Pid_motor;

// Pid_motor Pid_motor_t={
//	 50000.0,
//	 15000.0,
//	 0.0,
//	 0.0067
// };

 




void Motor_init(void)
{
//	  uart_init(USART_1,115200,UART1_TX_B6,UART1_RX_B7);    //串口初始用来看脉冲
	
	    qtimer_quad_init(QTIMER_2,QTIMER2_TIMER0_C0,QTIMER2_TIMER1_C1);  //编码器初始化
      qtimer_quad_init(QTIMER_2,QTIMER2_TIMER2_C2,QTIMER2_TIMER3_C3);
	
//	  gpio_init(D7, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);          //电机初始化
//   	gpio_init(D6, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
//	  gpio_init(D5, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
//	  gpio_init(D4, GPO, 1, PULLUP_100K|SPEED_50MHZ|DSE_R0);
			pwm_init(PWM2_MODULE3_CHA_C28, 50, 3500); //舵机初始化
	
	    pwm_init(PWM2_MODULE0_CHB_D7, 17000, 0);
	    pwm_init(PWM2_MODULE0_CHA_D6, 17000, 0);
	    pwm_init(PWM2_MODULE1_CHB_D5, 17000, 0);
	    pwm_init(PWM2_MODULE1_CHA_D4, 17000, 0);	
	
}


void encoder_get(void)
{
	      L_SpeedArray[1]=L_SpeedArray[0];
	      R_SpeedArray[1]=R_SpeedArray[0];
	      L_SpeedArray[0] = -(qtimer_quad_get(QTIMER_2,QTIMER2_TIMER0_C0));				
	      R_SpeedArray[0] = qtimer_quad_get(QTIMER_2,QTIMER2_TIMER2_C2);
//	      encoder_l = -(qtimer_quad_get(QTIMER_2,QTIMER2_TIMER0_C0));	
//	      encoder_r = qtimer_quad_get(QTIMER_2,QTIMER2_TIMER2_C2);
        encoder_l = (L_SpeedArray[0]+L_SpeedArray[1])/2;  //130(固定pwm为15000)空载
	      encoder_r = (R_SpeedArray[0]+R_SpeedArray[1])/2;  //130固定pwm为15000)空载
//	      aver_encoder = (encoder_l + encoder_r)/2; 
	      
	      L_Speed=(encoder_l*perimeter)/((float)(wheel_pulse*cycle));   //3.3固定pwm为15000)空载
	      R_Speed=(encoder_r*perimeter)/((float)(wheel_pulse*cycle));   //3.3固定pwm为15000)空载
	      avert_speed=(L_Speed+R_Speed)/2.0f;
	      
//	      L_pwm=(L_Speed*init_pwm)/init_speed;
//	      R_pwm=(R_Speed*init_pwm)/init_speed;	       
	      	      	      
	      qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER0_C0);
	      qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER2_C2);
//	    PRINTF("%d\t",encoder_l);
//	    uart_putchar(USART_1,encoder_l);	    	
}

/////////////////////////////////////////////////////////////////
void Left_control(void)
{
	   L_error=speedl_set-L_Speed;
//	   L_error=speedl_set-avert_speed;
	   L_kp_value=motor_kp*L_error;
	   L_ki_value+=L_error*motor_ki;
	   if(L_ki_value>=18000)  L_ki_value=18000;
	   else if(L_ki_value<=-18000)  L_ki_value=-18000;
	   L_out=L_kp_value+L_ki_value;
	   if(L_out>=25000) L_out=25000;
	   else if(L_out<=-25000) L_out=-25000;
}

void Right_control(void)
{
	   R_error=speedr_set-R_Speed;
//	   R_error=speedr_set-avert_speed;
	   R_kp_value=motor_kp*R_error;
	   R_ki_value+=R_error*motor_ki;
	   if(R_ki_value>=18000)  R_ki_value=18000;
	   else if(R_ki_value<=-18000)  R_ki_value=-18000;
	   R_out=R_kp_value+R_ki_value;
	   if(R_out>=25000) R_out=25000;
	   else if(R_out<=-25000) R_out=-25000;
	   
}
///////////////////////////////////////////////////////////////////
void Left_control_t(void)
{
	 L_error_t[2]=L_error_t[1];
	 L_error_t[1]=L_error_t[0];
	 L_error_t[0]=speedl_set-L_Speed;
	 L_kp_value_t=motor_kp_t*(L_error_t[0]-L_error_t[1]);
	 L_ki_value_t=motor_ki_t*L_error_t[0];
	 L_kd_value_t=motor_kd_t*(L_error_t[0]-2*L_error_t[1]+L_error_t[2]);
	 if(L_ki_value_t>=3000) L_ki_value_t=3000;
	 if(L_ki_value_t<=-3000) L_ki_value_t=-3000;
	 L_error_sum=L_kp_value_t+L_ki_value_t+L_kd_value_t;
	 L_out=Speed_Base+L_error_sum;
}

void Right_control_t(void)
{
	 R_error_t[2]=R_error_t[1];
	 R_error_t[1]=R_error_t[0];
	 R_error_t[0]=speedr_set-R_Speed;
	 R_kp_value_t=motor_kp_t*(R_error_t[0]-R_error_t[1]);
	 R_ki_value_t=motor_ki_t*R_error_t[0];
	 R_kd_value_t=motor_kd_t*(R_error_t[0]-2*R_error_t[1]+R_error_t[2]);
	 if(R_ki_value_t>=3000) R_ki_value_t=3000;
	 if(R_ki_value_t<=-3000) R_ki_value_t=-3000;
	 R_error_sum=R_kp_value_t+R_ki_value_t+R_kd_value_t;
	 R_out=Speed_Base+R_error_sum;
}

/////////////////////////////////////////////////////////////////
void Differ_speed()
{  
	 angle_t=(servo_control-servo_center)/Dif;
	 if(angle_t<-50) {angle_t=-50;}
	 else if(angle_t>50) {angle_t=50;}
	 Dif_array[2]=Dif_array[1];
	 Dif_array[1]=Dif_array[0];
	 Dif_array[0]=avert_speed*tanf(angle_t*3.14f/180.0f)*EDS_Coefficient_t;
	
	 //左轮：V_left=V*(1-(tanα)*B/2L)    右轮：V_right=V*(1+(tanα)*B/2L)   B/2=7.75   L=21
	 if(angle_t>0)                //左转
		{
			speedr_set=speedr_set1+(Dif_array[0]*0.4f+Dif_array[1]*0.4f+Dif_array[2]*0.2f);
			speedl_set=speedl_set1-(Dif_array[0]*0.4f+Dif_array[1]*0.4f+Dif_array[2]*0.2f);
		}
		else if (angle_t<0)                    //右转
		{
			speedl_set=speedl_set1-(Dif_array[0]*0.4f+Dif_array[1]*0.4f+Dif_array[2]*0.2f);
			speedr_set=speedr_set1+(Dif_array[0]*0.4f+Dif_array[1]*0.4f+Dif_array[2]*0.2f);
		}
		else
		{
			speedr_set=speedl_set1;
			speedl_set=speedl_set1;
		}
}



void motor_control(void)
{
    encoder_get();
	  Differ_speed();
		Left_control();
		Right_control();
		if(L_out>=0)
		{
			pwm_duty(PWM2_MODULE0_CHB_D7, L_out);
		  pwm_duty(PWM2_MODULE0_CHA_D6, 0);
		}
		else
		{
			pwm_duty(PWM2_MODULE0_CHB_D7, 0);
		  pwm_duty(PWM2_MODULE0_CHA_D6, -L_out);
		}
		if(	R_out>=0)
		{
			pwm_duty(PWM2_MODULE1_CHB_D5, 0);
		pwm_duty(PWM2_MODULE1_CHA_D4, R_out);	
		}
		else
		{
			pwm_duty(PWM2_MODULE1_CHB_D5, -R_out);
		   pwm_duty(PWM2_MODULE1_CHA_D4, 0);	
		}



//    pwm_duty(PWM2_MODULE0_CHB_D7, 15000);
//		  pwm_duty(PWM2_MODULE0_CHA_D6, 0);
//			pwm_duty(PWM2_MODULE1_CHB_D5, 0);
//		pwm_duty(PWM2_MODULE1_CHA_D4, 15000);	
}



void encoder_get2(void)
{
	      
	      encoder_l1 = -(qtimer_quad_get(QTIMER_2,QTIMER2_TIMER0_C0));				
	      encoder_r1 = qtimer_quad_get(QTIMER_2,QTIMER2_TIMER2_C2);
	      qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER0_C0);
	      qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER2_C2);
//	    PRINTF("%d\t",encoder_l);
//	    uart_putchar(USART_1,encoder_l);	    	
}












//跟踪-微分器
//void Dif_Tracker(float *u1,float *u2,float error)
//{
//	static float T=0.02,h=0.3,r=5000;   // T 周期   h  步长  r  ？？？ 
//	
//	float x1,x2,delta,delta0,y,abs_y,a0,a,fst;
//	
//	x1=*u1;
//	x2=*u2; 
//		
//    delta = r*h;
//    delta0 = delta*h;
//    y = x1-error+h*x2;
//    abs_y = fabsf(y);
//    a0 = sqrtf(delta*delta+8*r*abs_y);
//    if (abs_y <= delta0)
//	{
//		a = x2+y/h;
//    }
//	else
//	{
//		a = x2 + 0.5f*(a0-delta)*sign(y);
//	}
//    if (fabsf(a) <= delta)
//	{
//        fst = -r*a/delta;
//	}
//    else
//	{
//        fst = -r*sign(a);
//	}
//    *u1 = x1+T*x2; //跟踪数据
//    *u2 = x2+T*fst; //微分数据
//}

