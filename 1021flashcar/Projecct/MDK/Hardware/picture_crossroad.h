#ifndef _picture_crossroad_H

#define _picture_crossroad_H

#include <stdint.h> //用于uint8_定义

void crossroad_check(void);
void out_of_crossroad(void);




extern int Right_angle_pointY_down;
extern int Right_angle_pointX_down;
extern int Right_angle_pointX_up;
extern int Right_angle_pointY_up;
extern int Left_angle_pointX_up;
extern int Left_angle_pointY_up;
extern int Left_angle_pointX_down;
extern int Left_angle_pointY_down;
//入环标志
extern uint8 cross_flag;
extern uint8 cross_process;
//出环
extern uint8 cross_out_x ;
extern uint8 cross_out_y ;
extern uint8 cross_out_flag ;


extern uint8	Cross_right_down_flag;
extern uint8	Cross_left_down_flag;
extern uint8	Cross_right_up_flag;
extern uint8	Cross_left_up_flag;
#endif