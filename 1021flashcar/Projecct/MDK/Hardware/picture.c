#include "picture.h"
#include "SEEKFREE_MT9V032.h"
#include "picture_circle.h"
#include "picture_crossroad.h"


extern uint8 image[ROW][COL];
uint8_t image_IterativeThSegment[ROW][COL]; //用来存放迭代法后的图像
int Binaryzation_Value=60;  //迭代法中的阈值
int Otsu_value=0;  //大津法中的阈值
int Gray_Count[256];		//灰度直方图统计

uint8_t image_binaryzation[ROW][COL]; //用来存放sobel算子后的二值化图像
uint8_t Right_Black_boundary[ROW];  //用来存放右边界在某一行的那一列。Sample:Right_Black_boundary[2]=3即为第二行第三列为黑点
uint8_t Left_Black_boundary[ROW]; //用来存放左边界
uint8_t Record_LeftLine[ROW];  //用来存放标准右边界
uint8_t Record_RightLine[ROW]; //用来存放标准左边界
uint8_t middle_line[ROW][COL];

uint8 L_lost_height[2];
uint8 R_lost_height[2];
uint8 Record_middleline[ROW];
float angle[3];
float LeastSquare_b=0;
float LeastSquare_k[3]; //最小二乘法每20行拟合出一条直线的斜率，也就是三条直线的斜率
int left_boundary_sum=0;
int right_boundary_sum=0;


//有效行
uint8 useful_line=60; 
uint8 last_useful_line=0;
//有效行辅助判断
int useful_start = 10;
const uint8 Left_Line=1;
const uint8 Right_Line=COL-2;
const uint8 Bottom_Line=ROW-2;
const uint8 Top_Line=1;
int boundary_Line_last = COL/2;

int  lost_left_first = 0;
int  lost_right_first = 0;
int  lost_left_end = 0;
int  lost_right_end = 0;
int lost_right_left=0; //左右都丢线
int half_roadwidth[ROW]={13,13,13,14,14,15,15,15,16,16,17,
                        17,17,18,18,19,19,19,20,20,20,21,
                        21,22,22,22,23,23,23,24,24,25,25,
                        26,26,26,27,27,28,28,28,29,29,30,
                        31,32,32,32,33,33,34,34,34,35,35,
                        36,36,37,37,37,38,38,39,39,39,40,
                        39,39,40,40};
//动态路宽
int road_width=0;
//根据给定的特定赛道底部宽度和顶部宽度纯计算各行赛道宽度，与图像无关
uint8 Road_Width[ROW];
uint8 Road_Width_P10[ROW];
uint8 Road_Width_N10[ROW];												
//圆环
int left_circel_flag[5]; //左圆环标志
//十字路口
int crossroad_flag=0;
float angle_error;//用于最小二乘法算中线与正中偏差角
float average_error[3]={0};
//求左右边界的斜率
float k_r1,k_l1;

//迭代法采用逼近思想来求阈值
void IterativeThSegment(void)
{
	int i,j;
	int T0=0,T1=0,Z1=0 ,Z2=0,Z1_Count=0,Z2_Count=0; //T0为旧阈值，T1为新阈值,Z1为大于阈值的图层的像素和,Z2为小于阈值的图层的像素和
	for ( i = 0; i < 256; i++)
       Gray_Count[i]=0; //Gray_Count数组用来储存灰度值统计直方图

  for ( i = 0; i < 70; i++)  //行循环加列循环用来统计灰度直方图  Sample:Gray_Count[2]=5代表原图像中有五个像素值为2的点
    for ( j = 0; j < 144; j++)
    {
       Gray_Count[image[i][j]]++;
    }
	T0=Binaryzation_Value;  //直接设置初始阈值比较简便  //70
  for ( i = 0; i < 70; i++)
  {
    for ( j = 0; j < 144; j++)
    {
			if(image[i][j]>T0)
			{
				Z1+=image[i][j];  //统计大于阈值的图层的像素和
				Z1_Count++;				//统计大于阈值的图层的像素个数
			}
			else
			{
				Z2+=image[i][j];
				Z2_Count++;
			}
    }
  }
	T1=(Z1/Z1_Count+Z2/Z2_Count)/2;  //求出新阈值T1,公式为T1=(前景平均灰度值+背景平均灰度值)/2
	while(abs(T1-T0)>1 )  //容错范围为正负一
	{
		T0=T1;
		for ( i = 0; i < 70; i++)
		{
			for ( j = 0; j < 144; j++)
			{
				if(image[i][j]>T0)
				{
					Z1+=image[i][j];
					Z1_Count++;
				}
				else
				{
					Z2+=image[i][j];
					Z2_Count++;
				}
			}
		}
		T1=(Z1/Z1_Count+Z2/Z2_Count)/2;
	}
	if(T1<30)  //阈值太小则定为60
	{
		Binaryzation_Value=30;
	}
	else if(T1>110)  //阈值过大则定为110
	{
		Binaryzation_Value=110;
	}
	else  //阈值处于合适范围内，则认为准确
	{
		Binaryzation_Value=T1;
	}
	
	for ( i = 0; i < 70; i++)  //根据阈值将image二值化为image_IterativeThSegment
	{
		for ( j = 0; j < 144; j++)
		{
			if(image[i][j]>Binaryzation_Value)
			{
				image_IterativeThSegment[i][j]=255;
			}
			else
			{
				image_IterativeThSegment[i][j]=0;
			}
		}
	}
}

//最大协方差阈值法--即大津法
void OtsuThreshold(void)
{
	int i,j;
	float pixPro[256]={0};  //储存每个灰度值所占总像素比例
	//背景像素占比w1,前景像素占比w2,背/前景平均灰度值u1,u2
	float w0,w1,u0temp,u1temp,u,u0,u1,deltaTemp,deltaMax=0;
	for (i = 0; i < 256; i++)
			Gray_Count[i]=0; //Gray_Count数组用来储存灰度值统计直方图
	for(i=0;i<ROW;i++)
	{
		for(j=0;j<COL;j++)
		{
			Gray_Count[image[i][j]]++;  //统计每个灰度级中像素个数	
		}
	}
	for(i = 0; i < 256; i++)
	{
		pixPro[i]=Gray_Count[i]*1.0/10080;//计算每个灰度级像素数目占增幅图像像素比例	
	}
	for(i=0;i<256;i++) //遍历从0-255灰度级的阈值分割条件，测试哪一个类间方差最大
	{
		w0=w1=u0temp=u1temp=u0=u1=deltaTemp=0;
		for(j=0;j<256;j++)
		{
			if(j<=i) //背景
			{
				w0+=pixPro[j];
				u0temp += j*pixPro[j];
			}
			else  //前景
			{
				w1+=pixPro[j];
				u1temp += j*pixPro[j];
			}
		}
		u0=u0temp/w0;
		u1=u1temp/w1;
		deltaTemp = (int)(w0*w1*pow((u0-u1),2));//类间方差公式g=w1*w2*(u1-u2)^2
		if(deltaTemp>deltaMax)
		{
			deltaMax=deltaTemp;
			Otsu_value=i;
		}
	}
	
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			if(image[i][j]>Otsu_value)
			{
				image_IterativeThSegment[i][j]=White;
			}
			else
			{
				image_IterativeThSegment[i][j]=Black;
			}
		}
	}
}

void IterativeThSegment_boundary(void)
{
  int i,j;
	int Middle_line_last = COL/2;
	int temp=68;
//	Image_Lost();
	for(i=0;i<70;i++) Record_middleline[i]=0;
	for(i=0;i<70;i++)
	{
		for(j=0;j<144;j++) middle_line[i][j]=255;
	}

	
	for (j = temp; j>2; j--)
  {
		if (image_IterativeThSegment[j][Middle_line_last] == White)//若上一次的中线处于赛道中
		{
			//向左搜线
          for (i = Middle_line_last; i > 1; i--)
          {
              if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i - 1] == White && image_IterativeThSegment[j][i - 2] == Black) //即中间向左边搜线，遇到白白黑黑的时候为边界
              {
                  Left_Black_boundary[j] = i - 2;
							//	boundary_Line[j][i-2]=0;
                  break;
              }
              else
              {
                  Left_Black_boundary[j] = 1;
              }
          }
          //向右搜线
          for (i = Middle_line_last; i < Right_Line; i++)
          {
              if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i + 1] == White && image_IterativeThSegment[j][i + 2] == Black) //即中间向右边搜线，遇到白白黑黑的时候为边界
              {
                  Right_Black_boundary[j] = i + 2;
							//			boundary_Line[j][i+2]=0;
                  break;
              }
              else
              {
                  Right_Black_boundary[j] = Right_Line;
  
              }
          }
				}
		else
		{
		//向左搜线
          for (i = COL/2; i > 1; i--)
          {
              if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i - 1] == White && image_IterativeThSegment[j][i - 2] == Black ) //即中间向左边搜线，遇到白白黑黑的时候为边界
              {
                  Left_Black_boundary[j] = i - 2;
							//	boundary_Line[j][i-2]=0;
                  break;
              }
              else
              {
                  Left_Black_boundary[j] = 1;
  
              }
          }
          //向右搜线
          for (i = COL/2; i < Right_Line; i++)
          {
              if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i + 1] == White && image_IterativeThSegment[j][i + 2] == Black ) //即中间向右边搜线，遇到白白黑黑的时候为边界
              {
                  Right_Black_boundary[j] = i + 2;
								//		boundary_Line[j][i+2]=0;
                  break;
              }
              else
              {
                  Right_Black_boundary[j] = Right_Line;
  
              }
          }
				}
		Middle_line_last= (Right_Black_boundary[j]+ Left_Black_boundary[j])/2;
		   
					/*if(Left_Black_boundary[j] ==1&&Left_Black_boundary[j-1]==1&&Right_Black_boundary[j] != Right_Line&&Right_Black_boundary[j-1] != Right_Line) 
				{
					if((Right_Black_boundary[j]-(15+half_roadwidth[j]))<2)
					{
						middle_line[j][1]=0;
					Record_middleline[j]=1;
						boundary_Line_last =1;
				  }
					else
					{
						middle_line[j][Right_Black_boundary[j]-(15+half_roadwidth[j])]=0;
						Record_middleline[j]=Right_Black_boundary[j]-(15+half_roadwidth[j]);
					boundary_Line_last = Right_Black_boundary[j]-(15+half_roadwidth[j]);
					}
				}
				else if( Right_Black_boundary[j] == Right_Line&&Right_Black_boundary[j-1] == Right_Line&&Left_Black_boundary[j] !=1&&Left_Black_boundary[j-1]!=1)
				{
					
					if((Left_Black_boundary[j]+half_roadwidth[j]+15)>142)
					{
						middle_line[j][Right_Line]=0;
					Record_middleline[j]=Right_Line;
						boundary_Line_last = Right_Line;
				  }
					else
					{
					middle_line[j][Left_Black_boundary[j]+half_roadwidth[j]+15]=0;
					Record_middleline[j]=Left_Black_boundary[j]+half_roadwidth[j]+15;
					boundary_Line_last = Left_Black_boundary[j]+half_roadwidth[j]+15;
					}
				}
				else if(Left_Black_boundary[j] ==1&&Left_Black_boundary[j-1]==1&& Right_Black_boundary[j] == Right_Line&&Right_Black_boundary[j-1] == Right_Line)
				{
					middle_line[j][COL/2]=0;
					Record_middleline[j]=COL/2;
					boundary_Line_last = COL/2;
				}
				else
				{
					boundary_Line_last = (Right_Black_boundary[j] + Left_Black_boundary[j]) / 2;
					middle_line[j][boundary_Line_last]=0;	
					Record_middleline[j]=boundary_Line_last;
				}
				 LeastSquare();
				*/
      
		}
 }

void Image_Lost(void)//丢线寻找
{
	int i;
	uint8_t lost_temp_first=0,lost_temp_end=0;
  lost_left_first= 0;
  lost_right_first = 0;
  lost_left_end = 0;
  lost_right_end = 0;
  //第一次搜索丢线行
  for ( i = 50; i > 0; i--)
  {
      if (Left_Black_boundary[i] == 1 && Left_Black_boundary[i - 1] == 1)//连续两行找不到白白黑
      {
          lost_left_first = i--; //确定丢线的起始行
          
          while (Left_Black_boundary[i] == 1)
          {
              
              if (i == 0)
              {
                  i = -1;
                  break;
              }
              i--;
          }

          lost_left_end = i+1;

          break;
      }
  }

			for ( i = 50; i > 0; i--)
      {
          if (Right_Black_boundary[i] == COL - 2 && Right_Black_boundary[i - 1] == COL - 2)//如果右边丢线了
          {
              lost_right_first = i--;
              //通过遍历寻找该丢线区间的末端，用来作为识别十字上方拐点的起始行
              while (Right_Black_boundary[i] == COL - 2)
              {
                  
                  if (i == 0)
                  {
                      i = -1;
                      break;
                  }
                  i--;
              }
              lost_right_end = i + 1;
              
              break;
          }
      }
			
			for ( i = 68; i > 20; i--)
			{
					if (Left_Black_boundary[i] == 1 && Left_Black_boundary[i - 1] == 1)//如果左边丢线了
					{
							lost_temp_first = i--;
							
							//通过遍历寻找该丢线区间的末端，用来作为识别十字上方拐点的起始行
							while (Left_Black_boundary[i] == 1)
							{
									
									if (i == 0)
									{
											i = -1;
											break;
									}
									i--;
							}

							lost_temp_end = i+1;
							break;
					}
			}
			L_lost_height[1]=L_lost_height[0];
			L_lost_height[0]=lost_temp_first-lost_temp_end;
			for ( i = 68; i > 20; i--)
      {
          if (Right_Black_boundary[i] == COL - 2 && Right_Black_boundary[i - 1] == COL - 2)//如果右边丢线了
          {
              lost_temp_first = i--;
              //通过遍历寻找该丢线区间的末端，用来作为识别十字上方拐点的起始行
              while (Right_Black_boundary[i] == COL - 2)
              {
                  
                  if (i == 0)
                  {
                      i = -1;
                      break;
                  }
                  i--;
              }
              lost_temp_end = i + 1;
              
              break;
          }
      }
			R_lost_height[1]=R_lost_height[0];
			R_lost_height[0]=lost_temp_first-lost_temp_end;
			if(lost_left_first>lost_right_first) lost_right_left=lost_right_first;
			else lost_right_left=lost_left_first;
			
}
void useful_line_assist(void)
{
	int temp = 0;
	int i,j;
	for ( i = 40; i > 9; i--)
	{
		temp = (Right_Black_boundary[i] + Left_Black_boundary[i]) / 2;//中线

		if (image_IterativeThSegment[i - 1][temp - 1] == Black || image_IterativeThSegment[i - 1][temp + 1] == Black || i == 10)//如果中线往上是黑色
		{
			useful_start = i;
			for ( j = i; j < 35; j++)
			{
				if (Left_Black_boundary[j] == Left_Line)
				{
					useful_start = j;
					break;
				}
				if (Right_Black_boundary[j] == Right_Line)
				{
					useful_start = j;
					break;
				}
			}
			break;
		}
	}
}

void Useful_Track(void)//有效行搜索
{
	useful_line_assist();// 0.009ms
	
	last_useful_line=useful_line;
	useful_line=useful_start;
}

void LeastSquare(void)  //三段最小二乘法
{
	int i;
	float sum_x2 = 0, sum_x = 0, sum_y = 0, sum_xy = 0;
		for(i=11;i<31;i++)
		{
			sum_xy+=i*Record_middleline[i];
			sum_x+=i;
			sum_y+=Record_middleline[i];
			sum_x2+=i*i;
		}
		LeastSquare_k[0]=(sum_xy*20-sum_x*sum_y)/(sum_x2*20-sum_x*sum_x);
		angle[0]=atan(LeastSquare_k[0])*180/3.14;
		sum_x2 = 0; sum_x = 0; sum_y = 0; sum_xy = 0;
			for(i=31;i<51;i++)
		{
			sum_xy+=i*Record_middleline[i];
			sum_x+=i;
			sum_y+=Record_middleline[i];
			sum_x2+=i*i;
		}
			LeastSquare_k[1]=(sum_xy*20-sum_x*sum_y)/(sum_x2*20-sum_x*sum_x);
			angle[1]=atan(LeastSquare_k[1])*180/3.14;
			sum_x2 = 0; sum_x = 0; sum_y = 0; sum_xy = 0;
			for(i=31;i<51;i++)
		{
			sum_xy+=i*Record_middleline[i];
			sum_x+=i;
			sum_y+=Record_middleline[i];
			sum_x2+=i*i;
		}
			LeastSquare_k[1]=(sum_xy*20-sum_x*sum_y)/(sum_x2*20-sum_x*sum_x);
			angle[1]=atan(LeastSquare_k[1])*180/3.14;
			sum_x2 = 0; sum_x = 0; sum_y = 0; sum_xy = 0;
			for(i=50;i<60;i++)
		{
			sum_xy+=i*Record_middleline[i];
			sum_x+=i;
			sum_y+=Record_middleline[i];
			sum_x2+=i*i;
		}
			LeastSquare_k[2]=(sum_xy*10-sum_x*sum_y)/(sum_x2*10-sum_x*sum_x);
			angle[2]=atan(LeastSquare_k[2])*180/3.14;
			angle_error=angle[0]*0.2+angle[1]*0.4+angle[2]*0.4;
			sum_x2 = 0; sum_x = 0; sum_y = 0; sum_xy = 0;
	
}

void Addline_image(void)
{
	int i,j;
			if (cross_flag)
		{
			//十字补线
				switch (cross_flag)
				{
						case 0:
								break;
						case 1:
								for (i = Left_angle_pointX_down; i > Left_angle_pointX_up; i--)
								{
										Left_Black_boundary[i] = twopoints_Addline(Left_angle_pointX_up, Left_angle_pointX_down, Left_angle_pointY_up, Left_angle_pointY_down, i);
								}
								for ( i = Right_angle_pointX_down; i > Right_angle_pointX_up; i--)
								{
										Right_Black_boundary[i] = twopoints_Addline(Right_angle_pointX_down, Right_angle_pointX_up, Right_angle_pointY_down, Right_angle_pointY_up, i);
								}
								break;
						case 2:
								for ( i = Left_angle_pointX_up; i < 68; i++)
								{
										Left_Black_boundary[i] = Left_angle_pointY_up;
								}
								for ( i = Right_angle_pointX_up; i < 68; i++)
								{
										Right_Black_boundary[i] = Right_angle_pointY_up;
								}
								break;
						case 3:
								for ( i = Right_angle_pointX_down; i > Right_angle_pointX_up; i--)
								{
										Right_Black_boundary[i] = twopoints_Addline(Right_angle_pointX_down, Right_angle_pointX_up, Right_angle_pointY_down, Right_angle_pointY_up, i);
								}
								for ( i = Left_angle_pointX_down; i > Left_angle_pointX_up - 5; i--)
								{
										Left_Black_boundary[i] = Right_Black_boundary[i] - (2*half_roadwidth[i]);//路宽特殊处理，方便出十字
								}
								break;
						case 4:
								for ( i = Left_angle_pointX_down; i > Left_angle_pointX_up; i--)
								{
										Left_Black_boundary[i] = twopoints_Addline(Left_angle_pointX_down, Left_angle_pointX_up, Left_angle_pointY_down, Left_angle_pointY_up, i);
								}
								for ( i = Right_angle_pointX_down; i > Right_angle_pointX_up - 5; i--)
								{
										Right_Black_boundary[i] = Left_Black_boundary[i] + (2*half_roadwidth[i]);
								}
								break;
					    case 5:
								for ( i = Left_angle_pointX_down; i > 10; i--)
								{
									Left_Black_boundary[i] = Left_angle_pointY_down;
								}
								for ( i = Right_angle_pointX_down; i > 10; i--)
								{
									Right_Black_boundary[i] = Right_angle_pointY_down;
								}
								break;
				}
		
		
		
		}
		//圆环补线
 if (circle_flag == 1)
   {
       switch (circle_process)
       {
           //圆环
           case 1:
               {
								 if(Lcircle_pointx[2]!=0 && Lcircle_pointx[0]!=0)
								 {
                   for (int i = Lcircle_pointy[0]; i > Lcircle_pointy[2]; i--)//对2个拐点间的边界进行补线
                   {
                       Left_Black_boundary[i] = twopoints_Addline(Lcircle_pointx[0], Lcircle_pointx[2], Lcircle_pointy[0], Lcircle_pointy[2], i);
                   }
								 }
								else
								 {
									 int calculate_temp=0;
									 for (int i = Bottom_Line; i > 5; i--)//对2个拐点间的边界进行补线
                   {
										calculate_temp=Right_Black_boundary[i]-Road_Width[i];
										 if(calculate_temp>Left_Line)
										 {
											Left_Black_boundary[i] = Right_Black_boundary[i]-Road_Width[i];
										 }
										 else
										 {
											 Left_Black_boundary[i]=Left_Line;
										 }
                   }
								 }
									 
               }
               break;
           case 2:
               {
								 int calculate_temp=0;
                   //第二阶段的环岛用右边线的线补左边界的线
                   for (int i = Bottom_Line; i > Lcircle_pointx[2]; i--)//对2个拐点间的边界进行补线
                   {
//                       Left_Black_boundary[i] = Addline(circle_pointx[2], Left_Black_boundary[118], circle_pointy[2], 118, i);
										 calculate_temp=Right_Black_boundary[i]-Road_Width_P10[i];
										 if(calculate_temp>Left_Line)
										 {
                       Left_Black_boundary[i] = calculate_temp;
										 }
										 else
										 {
											 Left_Black_boundary[i]=Left_Line;
										 }
                   }
               }
               break;
           case 3:
               {
								 int up;
                   //第三阶段，搜索到点后补右边界的线
										int cal_temp;
										//y=a*x^2
										for (int i = Bottom_Line; i >= circle_X; i--)//对2个拐点间的边界进行补线
										{
//											Right_Black_boundary[i] = Addline(circle_X, Right_Black_boundary[Bottom_Line-5], circle_Y, Bottom_Line-5, i);
											cal_temp=Left_Black_boundary[i]+Road_Width[i];
											if(cal_temp<Right_Line)
											{
												Right_Black_boundary[i] = cal_temp;
											}
											else
											{
												Right_Black_boundary[i] = Right_Line;
											}
												
										}

               }
               break;
           case 4:
               {
										if(Big_circle_flag==1 && circle_out_static==0)
										{
											int cal_temp;
											for (int i = Bottom_Line; i > lost_right_end; i--)//对2个拐点间的边界进行补线
											{
												cal_temp=Left_Black_boundary[i]+Road_Width[i];
												if(cal_temp<Right_Line)
												{
													Right_Black_boundary[i] = cal_temp;
												}
												else
												{
													Right_Black_boundary[i] = Right_Line;
												}
													
											}
										}
										if(circle_out_static==1)
										{
											if(Big_circle_flag==1)
											{
												for (int i = Bottom_Line; i > 20; i--)//对2个拐点间的边界进行补线
												 {
														 Right_Black_boundary[i] = twopoints_Addline(0, Bottom_Line,20, Right_Black_boundary[Bottom_Line],  i);
												 }
											}
											else
											{
												 for (int i = Bottom_Line; i > lost_left_end; i--)//对2个拐点间的边界进行补线
												 {
														 Right_Black_boundary[i] = twopoints_Addline( lost_left_end, Bottom_Line,Left_Line+10, Right_Black_boundary[Bottom_Line], i);
												 }
										  }
										}
               }
               break;
           case 5:
               {
								 int calculate_temp=0;
                  for (int i = Bottom_Line; i > 10; i--)//
                  {
										calculate_temp=Right_Black_boundary[i]-Road_Width[i];
										if(calculate_temp>Left_Line)
										{
                      Left_Black_boundary[i] = calculate_temp;
										}
										else
										{
											Left_Black_boundary[i]=Left_Line;
										}
                  }
               }
               break;
       }
   }
	 
	 if (circle_flag == 2)
   {
       switch (circle_process)
       {
           //圆环
           case 1:
               {
									 if(Rcircle_pointy[2]!=0 && Rcircle_pointy[1]!=0)
									 {
										 for (int i = Rcircle_pointx[1]; i > Rcircle_pointx[2]; i--)//对2个拐点间的边界进行补线
										 {
												 Right_Black_boundary[i] = twopoints_Addline( Rcircle_pointy[1], Rcircle_pointy[2], Rcircle_pointx[1], Rcircle_pointx[2],i);
										 }
										}
									 else
									 {
										 int calculate_temp=0;
										 for (int i = Bottom_Line; i > 5; i--)//对2个拐点间的边界进行补线
										 {
												calculate_temp=Left_Black_boundary[i]+Road_Width[i];
											 if(calculate_temp<Right_Line)
											 {
												 Right_Black_boundary[i] = calculate_temp;
											 }
											 else
											 {
												 Right_Black_boundary[i]=Right_Line;
											 }
										 }
									 }
               }
               break;
           case 2:
               {
									int calculate_temp=0;
                   //第二阶段的环岛用左边线的线补右边界的线
                   for (int i = Bottom_Line; i > Rcircle_pointx[2]; i--)//对2个拐点间的边界进行补线
                   {
										 calculate_temp=Left_Black_boundary[i]+Road_Width_P10[i];
										 if(calculate_temp<Right_Line)
										 {
                       Right_Black_boundary[i] = calculate_temp;
										 }
										 else
										 {
											 Right_Black_boundary[i]=Right_Line;
										 }                  
									 }
									 
               }
               break;
           case 3:
               {
                  //第三阶段，搜索到点后补右边界的线
										int cal_temp;
										for (int i = Bottom_Line; i >= circle_X; i--)//对2个拐点间的边界进行补线
										{
//										Left_Black_boundary[i] = Addline(circle_X, Left_Black_boundary[Bottom_Line-5], circle_Y, Bottom_Line-5, i);
											cal_temp=Right_Black_boundary[i]-Road_Width[i]-15;
											if(cal_temp>Left_Line)
											{
												Left_Black_boundary[i] = cal_temp;
											}
											else
											{
												Left_Black_boundary[i] = Left_Line;
											}
												
										}
               }
               break;
           case 4:
               {
								 if(Big_circle_flag==1 && circle_out_static==0)
								 {
									 int cal_temp;
										for (int i = Bottom_Line; i > lost_left_end; i--)//对2个拐点间的边界进行补线
										{
											cal_temp=Right_Black_boundary[i]-Road_Width[i];
											if(cal_temp>1)
											{
												Left_Black_boundary[i] = cal_temp;
											}
											else
											{
												Left_Black_boundary[i] = Left_Line;
											}
										}
											
								 }
								 
								 //向右补线
									if(circle_out_static==1)
									{
										if(Big_circle_flag==1)
										{
											for (int i = Bottom_Line; i > 20; i--)//对2个拐点间的边界进行补线
											 {
													 Left_Black_boundary[i] = twopoints_Addline(0, Bottom_Line-20, Right_Line-20, Left_Black_boundary[Bottom_Line-20], i);
											 }
										}
										else
										{
											 for (int i = Bottom_Line; i > lost_right_end; i--)//对2个拐点间的边界进行补线
											 {
													 Left_Black_boundary[i] = twopoints_Addline(lost_right_end, Bottom_Line,Right_Line-10, Left_Black_boundary[Bottom_Line], i);
											 }
										}
									}
               }
               break;
           case 5:
               {
								 int calculate_temp=0;
                  for (int i = Bottom_Line; i > 10; i--)//进行斜率补线
                  {
										calculate_temp=Left_Black_boundary[i]+Road_Width[i];
										if(calculate_temp<Right_Line)
                      Right_Black_boundary[i] = calculate_temp;
										else
											Right_Black_boundary[i]=Right_Line;
                  }
               }
               break;
       }
		 }	
		//补线完成后再计算中线和最小二乘法
		 road_width=0;
		for(j=Bottom_Line;j>9;j--)
		{
			//j<=50是因为是从50行向上搜丢线行,不加的话68-60行左右的road_width是0，中线会跑到边界上
				if(j<=50&&Left_Black_boundary[j] ==1&&Left_Black_boundary[j-1]==1&&Right_Black_boundary[j] != Right_Line&&Right_Black_boundary[j-1] != Right_Line) 
				{
						road_width=Right_Black_boundary[lost_left_first]-Left_Black_boundary[lost_left_first];
					if((Right_Black_boundary[j]-(road_width/2))<2)
					{
						middle_line[j][1]=0;
					Record_middleline[j]=1;
						boundary_Line_last =1;
				  }
					else
					{
						/* 固定路宽数组
						middle_line[j][Right_Black_boundary[j]-(20+half_roadwidth[j])]=0;
						Record_middleline[j]=Right_Black_boundary[j]-(20+half_roadwidth[j]);
					boundary_Line_last = Right_Black_boundary[j]-(20+half_roadwidth[j]);
						*/
						//动态路宽计算
							middle_line[j][Right_Black_boundary[j]-(road_width/2)]=0;
						Record_middleline[j]=Right_Black_boundary[j]-(road_width/2);
					boundary_Line_last = Right_Black_boundary[j]-(road_width/2);
						
					}
				}
				//j<=50是因为是从50行向上搜丢线行,不加的话68-60行左右的road_width是0，中线会跑到边界上
				else if(j<=50&&Right_Black_boundary[j] == Right_Line&&Right_Black_boundary[j-1] == Right_Line&&Left_Black_boundary[j] !=1&&Left_Black_boundary[j-1]!=1)
				{
					road_width=Right_Black_boundary[lost_right_first]-Left_Black_boundary[lost_right_first];
					
					if((Left_Black_boundary[j]+(road_width/2))>142)
					{
						middle_line[j][Right_Line]=0;
					Record_middleline[j]=Right_Line;
						boundary_Line_last = Right_Line;
				  }
					else
					{
					middle_line[j][Left_Black_boundary[j]+(road_width/2)]=0;

						Record_middleline[j]=Left_Black_boundary[j]+(road_width/2);
					boundary_Line_last = Left_Black_boundary[j]+(road_width/2);
					}
				}
//				else if(Left_Black_boundary[j] ==1&&Left_Black_boundary[j-1]==1&& Right_Black_boundary[j] == Right_Line&&Right_Black_boundary[j-1] == Right_Line)
//				{
//					middle_line[j][COL/2]=0;
//					Record_middleline[j]=COL/2;
//					boundary_Line_last = COL/2;
//				}
				else
				{
					boundary_Line_last = (Right_Black_boundary[j] + Left_Black_boundary[j]) / 2;
					middle_line[j][boundary_Line_last]=0;	
					Record_middleline[j]=boundary_Line_last;
				}
      }
			LeastSquare();
		}
//两点求直线后补线，以行为x,列为y，Sample:twopoints_Addline(Left_angle_pointY_down,Left_angle_pointY_up,Left_angle_pointX_up,Left_angle_pointX_up,i)
//要注意的是Left_angle_pointX_up中的X代表的是列，与这里建的系相反
uint8 twopoints_Addline(int x1,int x2,int y1,int y2,int x)  //根据两个点补全中间的线，用于十字补线
{
	float k=0,b=0;
	float y;
	k=(y2-y1);
	k=k/(x2-x1);
	b=y2-k*x2;
	y=k*x+b;
	if(y<=1) y=1;
	if(y>COL-2)  y=COL-1;
	return (uint8)y;
}
void middle_error(void)  //与绝对中线的差值做平均求出偏差
{
	int prospect_Line=40;  //从40行往下位置开始算偏差
	int i;
	int temp=20;
	for(i=21;i<41;i++)
	{
		if(image_IterativeThSegment[i][Record_middleline[i]]==White)
		{
			average_error[0]+=(Record_middleline[i]-72);
		}
		else
		{
			temp--;
		}
	}
	if(temp!=0) average_error[0]=average_error[0]/temp;
	temp=20;
	for(i=31;i<51;i++)
	{
	   if(image_IterativeThSegment[i][Record_middleline[i]]==White)
		{
			average_error[1]+=(Record_middleline[i]-72);
		}
		else
		{
			temp--;
		}
	}
	if(temp!=0) average_error[1]=average_error[1]/temp;
	temp=10;
	for(i=51;i<61;i++)
	{
	   if(image_IterativeThSegment[i][Record_middleline[i]]==White)
		{
			average_error[2]+=(Record_middleline[i]-72);
		}
		else
		{
			temp--;
		}
	}
	if(temp!=0) average_error[2]=average_error[2]/temp;

}
int Trend_Boundary(uint8* array, Trend trend)  //用于判断路宽突变，例如圆环宽变窄变宽(-文武)
{
    int Boundary_Length=0;
    int Trend_Error = 0;
		int temp_error;
		int i;
    for ( i=68;i>10;i--)
    {
			temp_error=array[i] - array[i - 1];
        if (trend == Right)
        {
            if ( temp_error>= 0)
            {
                Boundary_Length++;
            }
            else
            {
                Trend_Error++;
                if (Trend_Error > 2)
                {
                    break;
                }
            }
        }
        if (trend == Left)
        {
            if (temp_error <= 0)
            {
                Boundary_Length++;
            }
            else
            {
                Trend_Error++;
                if (Trend_Error > 2)
                {
                    break;
                }
            }
        }
    }
    return Boundary_Length;
}

void Get_RoadWidth(void)
{
	//梯形上底a，下底b，高h,所求平行线距离下底的距离m
	//宽度x=b-m(b-a)/h
	uint8 i;
	const int Bottom_Width=115;
	const int Top_Width=89;
	const int Hight=20;
	for( i=68;i>0;i--)
	{
		Road_Width[i]=Bottom_Width-(68-i)*(Bottom_Width-Top_Width)/(Hight);
		Road_Width_P10[i]=Road_Width[i]+10;
		Road_Width_N10[i]=Road_Width[i]-10;
	}
}
void LeastSquare_boundary(void)   //对左右边界的最小二乘法
{
			int i;
	float sum_x2 = 0, sum_x = 0, sum_y = 0, sum_xy = 0;
	int row_num=38;   //从第30行到第68行要求的行数
		for(i=30;i<Bottom_Line;i++)   //左边界斜率
		{
			if(Left_Black_boundary[i]!=1)
			{
				sum_xy+=i*Left_Black_boundary[i];
				sum_x+=i;
				sum_y+=Left_Black_boundary[i];
				sum_x2+=i*i;
			}
			else 
			{
				row_num--;
			}
		}
		k_l1=(sum_xy*row_num-sum_x*sum_y)/(sum_x2*row_num-sum_x*sum_x);
		sum_x2 = 0; sum_x = 0; sum_y = 0; sum_xy = 0;
		
		for(i=30;i<Bottom_Line;i++)   //右边界斜率
		{
			if(Right_Black_boundary[i]!=Right_Line)
			{
				sum_xy+=i*Right_Black_boundary[i];
				sum_x+=i;
				sum_y+=Right_Black_boundary[i];
				sum_x2+=i*i;
			}
		}	
		k_r1=(sum_xy*row_num-sum_x*sum_y)/(sum_x2*row_num-sum_x*sum_x);	
	
}
void Record_Boundary(void)
{
	for(int i=68;i>2;i--)
	{
		Record_LeftLine[i]=Left_Black_boundary[i];
		Record_RightLine[i]=Right_Black_boundary[i];
	}
}