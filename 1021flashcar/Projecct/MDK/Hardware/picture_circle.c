#include "picture_circle.h"
#include "picture.h"
#include "SEEKFREE_MT9V032.h"
#include "ips.h"


uint8 angle_pointx[4];
uint8 angle_pointy[4];
uint8 Rcircle_pointy[6];
uint8 Rcircle_pointx[6];
uint8 Lcircle_pointy[6];
uint8 Lcircle_pointx[6];
uint8 Trend_Left,Trend_Right;
//标志位声明
uint8 circle_out_static=0;//出环打死角标志位
uint8 circle_out=0;
uint8 circle_flag=0;//环岛标志位，用来定义左右环；1为左环，2为右环
uint8 circle_process=0;//环岛进程标志位，1~5代表入环到出环
uint8 Big_circle_flag=0;//大环岛标志位
///第一次入环的检测
int circle_first_Y = 0;
int circle_first_X = 0;
int circle_first_flag = 0;
int circle_X=4;
int circle_Y=COL/2;
uint8 circle_direction = 0;//左环1，右环2
uint8 Left_Edge[ROW] = {0};
uint8 Right_Edge[ROW] = {0};
uint8 line_flag=0;



void Circle_Search(void)//环岛综合识别
{
	static uint8 Lcircle_count=0,Rcircle_count=0;
//	 circle_process=4;
//	 circle_flag=1;
	 static int temp_flag1=0;
		 Trend_Left=Trend_Boundary(Left_Black_boundary,Left);
		 Trend_Right=Trend_Boundary(Right_Black_boundary,Right);
			LeastSquare_boundary();  //对左右边界进行最小二乘法计算
	 //初始化环岛数组
     for(int i=0;i<6;i++)
     {
         Lcircle_pointx[i] = 0;
         Lcircle_pointy[i] = 0;
				 Rcircle_pointx[i] = 0;
         Rcircle_pointy[i] = 0;
     }
		 
		 
		 circle_first();
			if(circle_first_flag==1 && circle_flag==0)
			{
				Lcircle_count++;
				if(Lcircle_count>=3)
				{
					circle_flag = 1;//标志为左圆环
					circle_process = 1;//圆环第一阶段
					//circle_count_now++;
					Lcircle_count=0;
				}
			}
			else
			{
				Lcircle_count=0;
			}
			if(circle_first_flag==2 && circle_flag==0)
			{
				Rcircle_count++;
				if(Rcircle_count>=3)
				{
					circle_flag = 2;//标志为左圆环
					circle_process = 1;//圆环第一阶段
				//	circle_count_now++;
					Rcircle_count=0;
				}
			}
			else
			{

				Rcircle_count=0;
			}
			
		 //进行左环岛的识别判定
		if(circle_flag==1) 
		{
			
			//环岛第二阶段的判定
      if(circle_process==1 && Lcircle_pointx[0] == 0 && lost_left_first==50)
      {
				circle_process=2;
				//Encoder_flag=1;//此时编码器开始积分
			}

		//环岛第二阶段：此时搜不到左下角拐点，通过环岛中的拐点进行补线处理，并寻找拐点上方的环岛角点
		 if(circle_process==2)
		 {
        for(int i=40;i>10;i--)
        {
            if(Left_Black_boundary[i]>  Left_Black_boundary[i - 2] && 
               Left_Black_boundary[i]>  Left_Black_boundary[i - 1] && 
               Left_Black_boundary[i]>= Left_Black_boundary[i + 1] &&
               Left_Black_boundary[i]>= Left_Black_boundary[i + 2] 
              )
            {
                Lcircle_pointy[2] = Left_Black_boundary[i];
                Lcircle_pointx[2] = i;
                break;
            }
        }
				circle_find_again();

				 if(circle_X>10)
				 {
							circle_process = 3;
//				 Encoder_Sumvalue=GetClr_EncoderSumValue();
//				 if(Encoder_Sumvalue>9500)
//						 {
//							 Big_circle_flag=1;//标记为大环
//						 } 
//				 
				 }
				 
				
		}
		 //环岛第三阶段：搜索入环的角点，并补右边线
     if(circle_process==3)
     {
//         for (int i = lost_left_first; i > 10; i--)
//         {
//             if (Right_Black_boundary[i] >= Right_Black_boundary[i - 1] + 10 &&
//							 Right_Black_boundary[i]+3>Right_Black_boundary[i+1])
//             {
//                 circle_pointx[3] = Right_Black_boundary[i - 1];
//                 circle_pointy[3] = i - 1;
//                 break;
//             }
//         }
				 circle_find_again();
				 if(circle_X>Bottom_Line-28 || circle_X==4)
				{
						circle_process = 4;

				}

     }
		 
     //环岛第四阶段：当在环岛里面的时候，现在开始搜索右边的角点
		 if(circle_process==4)
		 {
			 //搜索右边拐角
//			 if(lost_right_first!=lost_right_end && lost_right_first<100 )
//			 {
				 int lost_right_up = lost_right_first;
				 if (lost_right_up < 10) lost_right_up = 10;
				 static int temp_flag2=0;
				 if(lost_right_first!=50 && lost_right_first!=0)
				 { 
				 if(circle_out_static!=2)
				 {
						 for (int i = 64; i > lost_left_end; i--)
						 {
							if(Right_Black_boundary[i+2]<=Right_Black_boundary[i+4]&&
								 Right_Black_boundary[i+2]<=Right_Black_boundary[i+3]&&
								 Right_Black_boundary[i+2]<Right_Black_boundary[i]&&
								 Right_Black_boundary[i+2]<Right_Black_boundary[i+1] &&
								 i+2>30
								)
							{
								uint8 black_point_flag=0;//防止黑点出现
								for(int j=i+3;j<68;j++)
								{
									if(abs(Right_Black_boundary[j-1]-Right_Black_boundary[j])>20)
									{
										black_point_flag=1;
										break;
									}
								}
								
								if(black_point_flag==0 && 
									Right_Black_boundary[i+2]<=Right_Black_boundary[i+1] &&
									Right_Black_boundary[i+1]<=Right_Black_boundary[i] &&
									Right_Black_boundary[i]<=Right_Black_boundary[i-1])
								{
									Lcircle_pointx[4] = i+2;
									Lcircle_pointy[4] = Right_Black_boundary[i+2];
									circle_out=1;

									circle_out_static=1;

									break;
								}
							}

				 }
						}
			 }
				 if(circle_out_static==1 && lost_right_end>33)
				 {
						circle_out_static=2;
			   }
			 LeastSquare_boundary();//计算右边界斜率
				 if(Lcircle_pointx[4]!=0 && lost_right_first<=50)
				 {
					 temp_flag2=1;
				 }
				//环岛第五阶段：当搜不到右边拐点，且右边是没丢线的正常直道边界时，可以判断是第五阶段

			 if (Big_circle_flag==1 && Lcircle_pointx[4]==0 && circle_process==4 /*&& k_r1>-1.2f */&& k_r1>1.0f && k_r1<10.0f && lost_right_first==0 && Trend_Boundary(Right_Black_boundary,Right)>55 && temp_flag2==1)
			 {
					 circle_process = 5;
					 temp_flag2=0;
					 circle_out_static=0;
			 }
			 if (Big_circle_flag==0 && Lcircle_pointx[4]==0 && circle_process==4 /*&& k_r1>-1.2f */&& k_r1>1.0f && k_r1<10.0f && lost_right_first==0 && Trend_Boundary(Right_Black_boundary,Right)>55 && temp_flag2==1)
			 {
					 circle_process = 5;
					 temp_flag2=0;
					 circle_out_static=0;
			 }
		 }

		 
		 //圆环第五阶段，进行出环的补线，补左边界的线
		 if(circle_process==5)
		 {
//			 if(Encoder_flag==0 && k_r1<-0.5f)
//			 {
//				 Encoder_flag=1;
//				 Encoder_Sumvalue=0;
//			 }
			  uint8 lost_temp_first=0,lost_temp_end=0;
				for (int i = 63; i > 10; i--)
        {
            if(Left_Black_boundary[i]==1 && Left_Black_boundary[i-1]==1)
						{
							lost_temp_first=i;
							while (Left_Black_boundary[i] == 1)
							{
									
									if (i == 0)
									{
											i = -1;
											break;
									}
									i--;
							}

							lost_temp_end = i+1;
							break;
						}
        }
			 for(int i=lost_temp_end+5;i>10;i--)
			 {
				 if(Left_Black_boundary[i]==Left_Line &&
 					  Left_Black_boundary[i-1]==Left_Line && 
						Left_Black_boundary[i-2]>Left_Black_boundary[i-1]+5 &&
						abs(Left_Black_boundary[i-2]-Left_Black_boundary[i-3])<=3 &&
						Right_Black_boundary[i-1]-Left_Black_boundary[i-1]>Right_Black_boundary[i-2]-Left_Black_boundary[i-2]+10)
				 {
					 Lcircle_pointx[5]=i-2;
					 Lcircle_pointy[5]=Left_Black_boundary[i-2];
					 break;
				 }
			 }
			 static int temp_flag=0;
			 if(Lcircle_pointx[5]>30)
			 {
				 temp_flag=1;
			 }
			 if((temp_flag==1 && lost_temp_end>50))//||(Encoder_flag==1 && Encoder_Sumvalue>12000 && Big_circle_flag==0)||(Encoder_flag==1 && Encoder_Sumvalue>20000 && Big_circle_flag==1))
			 {
				 circle_process = 0;//成功出环
					circle_flag=0;//标志位清空
					line_flag=0;
					Big_circle_flag=0;
					temp_flag=0;
					circle_out=0;
//				 Encoder_flag=0;

			 }


			}
		
		}
		
		 //进行右环岛的识别判定
		if(circle_flag==2)
		{
			if(circle_process==1 && Rcircle_pointx[1] == 0 && lost_right_first==50)
      {
				circle_process=2;
//				Encoder_flag=1;//此时开始积分
			}
			
			//环岛第二阶段：此时搜不到右下角拐点，通过环岛中的拐点进行补线处理，并寻找拐点上方的环岛角点
		 if(circle_process==2)
		 {
        for(int i=40;i>5;i--)
        {
            if(Right_Black_boundary[i + 2] < Right_Black_boundary[i] &&
               Right_Black_boundary[i + 2] < Right_Black_boundary[i + 1] && 
               Right_Black_boundary[i + 2] <= Right_Black_boundary[i + 3] &&
               Right_Black_boundary[i + 2] <= Right_Black_boundary[i + 4]
//               Right_Black_boundary[i - 2] <= 177 &&
//							 Right_Black_boundary[i - 4] <  177 
              )
            {
                Rcircle_pointy[2] = Right_Black_boundary[i + 2];
                Rcircle_pointx[2] = i + 2;
                break;
            }
        }
				circle_find_again();
				if(circle_X>10)
				{
					circle_process = 3;
//					 Encoder_Sumvalue=GetClr_EncoderSumValue();
//					 if(Encoder_Sumvalue>9500)
//					 {
//						 Big_circle_flag=1;//标记为大环
//					 } 
				}
				//当为圆环的第二阶段的时候，开始搜圆环的第三个点
//				if(circle_process==2 && (Rcircle_pointy[2] > 50 || Rcircle_pointx[2]>102) && (lost_right_first!=50)) 
//				 {
//						 circle_process = 3;
//						 Encoder_Sumvalue=GetClr_EncoderSumValue();
//						 if(Encoder_Sumvalue>9500)
//						 {
//							 Big_circle_flag=1;//标记为大环
//						 }
//				 }
				
		}
		//环岛第三阶段：搜索入环的角点，并补左边线	
		 if(circle_process==3)
     {

				 circle_find_again();
				 if(circle_X>Bottom_Line-28 || circle_X==4)
				{
						circle_process = 4;
				}

     }
		 
		 //环岛第四阶段：当在环岛里面的时候，现在开始搜索左边的角点
		 if(circle_process==4)
		 {
			 uint8 str[10];
	 
			 //搜索左边拐角
//			 if(lost_left_first!=lost_left_end && lost_left_first<100 && lost_left_first-lost_left_end>10)
//			 {
				 int lost_left_up = lost_left_first;
				 if (lost_left_up < 10) lost_left_up = 10;
				
				 static int temp_flag2=0;
				 if(circle_out_static!=2)
				 {
					 for (int i = 64; i > lost_right_end; i--)
					 {

							 //搜索上面的点
						 if(lost_left_first!=50 && lost_left_first!=0)
						 {
							if(Left_Black_boundary[i+2]>=Left_Black_boundary[i+4]&&
								 Left_Black_boundary[i+2]>=Left_Black_boundary[i+3]&&
								 Left_Black_boundary[i+2]<=Left_Black_boundary[i+3]+5&&
								 Left_Black_boundary[i+2]> Left_Black_boundary[i]&&
								 Left_Black_boundary[i+2]> Left_Black_boundary[i+1] &&
								 i+2>30
								 )
							{
								uint8 black_point_flag=0;//防止黑点出现
								for(int j=i+3;j<68;j++)
								{
									if(abs(Left_Black_boundary[j-1]-Left_Black_boundary[j])>20)
									{
										black_point_flag=1;
										break;
									}
								}
								if(black_point_flag==0 &&
									Left_Black_boundary[i+2]>= Left_Black_boundary[i+1] && 
									Left_Black_boundary[i+1]>= Left_Black_boundary[i] &&
									Left_Black_boundary[i]>= Left_Black_boundary[i-1])
								{
									Rcircle_pointx[4] = i+2;
									Rcircle_pointy[4] = Left_Black_boundary[i+2];
									circle_out=1;
									circle_out_static=1;
									break;
								}
								
							}
						}

					 }
					}
				 if(circle_out_static==1 /*&& Rcircle_pointx[4]>=48*/&&lost_left_end>33)
				 {
						circle_out_static=2;
			   }

				LeastSquare_boundary();//计算左边界斜率
			 //环岛第五阶段：当搜不到左边拐点，且左边是没丢线的正常直道边界时，可以判断是第五阶段
				 if(Rcircle_pointx[4]!=0 && lost_left_first<=50)
				 {
					 temp_flag2=1;
				 }
//				 	LCD_CursorFill(1,6,8*16,8,WHITE); 
//	sprintf((char *)str,"%d,%.3f",temp_flag2,k_l1);
//	LCD_WriteString(1,6,str);
			 if (Big_circle_flag==1 && Rcircle_pointx[4]==0 && circle_process==4 && /*k_l1<1.3f &&*/ k_l1<-1.0f && k_l1>-10.0f && lost_left_first==0  && temp_flag2==1 && Trend_Boundary(Left_Black_boundary,Left)>55)
			 {
					 circle_process = 5;
					 temp_flag2=0;
					 circle_out_static=0;
			 }
			 if (Big_circle_flag==0 && Rcircle_pointx[4]==0 && circle_process==4 && /*k_l1<1.3f &&*/ k_l1<-1.0f && k_l1>-10.0f && lost_left_first==0 && temp_flag2==1 && Trend_Boundary(Left_Black_boundary,Left)>55)
			 {
					 circle_process = 5;
					 temp_flag2=0;
					 circle_out_static=0;
			 }
		 }
		 //圆环第五阶段，进行出环的补线，补右边界的线
		 if(circle_process==5)
		 {
			uint8 lost_temp_first=0,lost_temp_end=0;
//			 if(Encoder_flag==0 && k_l1>0.5f)
//			 {
//				 Encoder_flag=1;
//				 Encoder_Sumvalue=0;
//			 }
				for (int i = 63; i > 10; i--)
        {
            if(Right_Black_boundary[i]==COL-2 && Right_Black_boundary[i-1]==COL-2)
						{
							lost_temp_first=i;
							while (Right_Black_boundary[i] == COL-2)
							{
									
									if (i == 0)
									{
											i = -1;
											break;
									}
									i--;
							}

							lost_temp_end = i+1;
							break;
						}
        }
			 for(int i=lost_temp_end+5;i>10;i--)
			 {
				 if(Right_Black_boundary[i]==Right_Line &&
 					  Right_Black_boundary[i-1]==Right_Line && 
						Right_Black_boundary[i-2]+5<Right_Black_boundary[i-1] &&
						abs(Right_Black_boundary[i-2]-Right_Black_boundary[i-3])<=3 &&
						Right_Black_boundary[i-1]-Left_Black_boundary[i-1]>Right_Black_boundary[i-2]-Left_Black_boundary[i-2]+10)
				 {
					 Rcircle_pointx[5]=i-2;
					 Rcircle_pointy[5]=Right_Black_boundary[i-2];
					 break;
				 }
			 }
			static int temp_flag=0;
			 if(Rcircle_pointy[5]>30)
			 {
				 temp_flag=1;
			 }
			 if((temp_flag==1 && lost_temp_end>50))// || (Encoder_flag==1 && Encoder_Sumvalue>12000 && Big_circle_flag==0)||(Encoder_flag==1 && Encoder_Sumvalue>20000 && Big_circle_flag==1))
			 {
					circle_process = 0;//成功出环
					circle_flag=0;//标志位清空
					line_flag=0;
					Big_circle_flag=0;
					temp_flag=0;
					circle_out=0;
//				  Encoder_flag=0;
//				 	circle_count_now--;

			 }
			 
			}
		 
		}
}

void circle_first(void)
{
	int temp = 0;
	int i,j;
	const int point_edge_judge = 18;  //角点处突变的路宽要大于这个值
	const int black_point_judge = 18;
	circle_first_Y = 0;
	circle_first_X = 0;
	circle_first_flag = 0;
	//左环
	for ( i = Bottom_Line-20; i > 13; i--)
	{
		if (Left_Black_boundary[i] - Left_Black_boundary[i - 2] > point_edge_judge ||
			Left_Black_boundary[i] - Left_Black_boundary[i - 1] > point_edge_judge)
		{
			circle_first_Y = Left_Black_boundary[i];
			circle_first_X = i;
			circle_first_flag = 1;
			if (circle_first_Y > COL / 2)
			{
				circle_first_flag = 0;
			}                    
			break;
		}
	}

	//2. 另一边的边界特征
    if (circle_first_flag != 0)
	{
		temp = 0;
		// i + 3 和 Top_Line+3有效解决误判
		for ( i = 10; i < Bottom_Line - 20; i++)
		{
			if (Right_Black_boundary[i] > Right_Black_boundary[i + 1] ||
				abs(Right_Black_boundary[i] - Right_Black_boundary[i + 3]) > 6)
			{
				temp++;
			}
			if (temp > 1)
			{
				circle_first_flag = 0;
				break;
			}
		}
	}
	//3. 路宽特征 ！！！当摄像头位置移动了要记得重新设定Road_Width
	if (circle_first_flag != 0)
	{
		temp = Right_Black_boundary[circle_first_X] - Left_Black_boundary[circle_first_X];
		if (temp <= Road_Width[circle_first_X] - 10 || temp >= Road_Width[circle_first_X] + 10)
		{
			circle_first_flag = 0;
		}
	}
	//4. 点上为白
	if (circle_first_flag != 0)
	{
		temp = 0;
		for ( i = circle_first_Y - 10; i < circle_first_Y + 10; i++)
		{
			for ( j = circle_first_X - 2; j > circle_first_X - 7; j--)
			{
				if (image_IterativeThSegment[j][i] == Black)
				{
					temp++;
				}
			}
			if (temp > 0)
			{
				circle_first_flag = 0;
				break;
			}
		}
	}
	//5. 点周围黑点个数（图像极端）
	/*if (circle_first_flag != 0)
	{
		temp = 0;
		for ( i = circle_first_X-1; i < circle_first_X + 4; i++)
		{
			for ( j = circle_first_Y - 1; j > circle_first_Y - 8; j--)
			{
				if (image_IterativeThSegment[i][j] == Black)
				{
					temp++;
				}
			}
		}
		if (temp < black_point_judge)
		{
			circle_first_flag = 0;
		}
	}
	*/
	if (circle_first_flag != 0)
	{
		return;
	}

	//右环
	for ( i = Bottom_Line-20; i > 15; i--)
	{
		if (Right_Black_boundary[i - 2] - Right_Black_boundary[i] > point_edge_judge ||
			Right_Black_boundary[i - 1] - Right_Black_boundary[i] > point_edge_judge)
		{
			circle_first_Y = Right_Black_boundary[i];
			circle_first_X = i;
			circle_first_flag = 2;
			if (circle_first_Y < COL / 2)
			{
				circle_first_flag = 0;
			}
			break;
		}
	}

	//2. 另一边的边界特征
    if (circle_first_flag == 2)
	{
		temp = 0;
		// i + 3
		for ( i = Top_Line + 8; i < Bottom_Line - 20; i++)
		{
			if (Left_Black_boundary[i] < Left_Black_boundary[i + 1] ||
				abs(Left_Black_boundary[i] - Left_Black_boundary[i + 3]) > 6)
			{
				temp++;
			}
			if (temp > 1)
			{
				circle_first_flag = 0;
				break;
			}
		}
	}
	
	//3. 路宽特征！！！当摄像头位置移动了要记得重新设定Road_Width
	if (circle_first_flag == 2)
	{
		temp = Right_Black_boundary[circle_first_X] - Left_Black_boundary[circle_first_X];
		if (temp < Road_Width[circle_first_X] - 10 || temp >Road_Width[circle_first_X]+ 10)
		{
			circle_first_flag = 0;
		}
	}
	
	//4. 点上为白
	if (circle_first_flag == 2)
	{
		temp = 0;
		for ( i = circle_first_Y - 10; i < circle_first_Y + 10; i++)
		{
			for ( j = circle_first_X - 2; j > circle_first_X - 7; j--)
			{
				if (image_IterativeThSegment[j][i] == Black)
				{
					temp++;
				}
			}
			if (temp > 0)
			{
				circle_first_flag = 0;
				break;
			}
		}
	}
	
	//5. 点周围黑点个数（图像极端）
	/*if (circle_first_flag == 2)
	{
		temp = 0;
		for ( i = circle_first_X-1; i < circle_first_X + 4; i++)
		{
			for ( j = circle_first_Y + 1; j < circle_first_Y + 8; j++)
			{
				if (image_IterativeThSegment[i][j] == Black)
				{
					temp++;
				}
			}
		}
		if (temp < black_point_judge)
		{
			circle_first_flag = 0;
		}
	}
*/
	if (circle_first_flag == 0)
	{
		circle_first_Y = 0;
		circle_first_X = 0;
	}

}

//已识别到圆环后放宽条件搜单点
void circle_find_again(void)
{	
	int circle_point_judge=15;
	int i,j;
	circle_direction=circle_flag;
	if(circle_process==2||circle_process==3)
	{
	}
	else
	{
		circle_Y=COL/2;
		circle_X=4;
		return;
	}

	//右环
	if (circle_direction == 2)
	{
		Left_Edge[Bottom_Line] = Left_Black_boundary[Bottom_Line];
		//向左搜线
		for ( j = Bottom_Line - 1; j > Top_Line+2; j--)
		{
			for ( i = Left_Edge[j + 1] + 10; i > Left_Line + 0; i--)
			{
				if (i < Right_Line && image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i - 1] == White && image_IterativeThSegment[j][i - 2] == Black)
				{
					Left_Edge[j] = i - 1;
					break;
				}
				else
				{
					Left_Edge[j] = Left_Line;
				}
			}
		}
		//用左搜右
		for ( j = Bottom_Line ; j > Top_Line+2; j--)
		{
			for ( i =  Left_Edge[j]; i < Right_Line - 2; i++)
			{
				if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i + 1] == White && image_IterativeThSegment[j][i + 2] == Black )
				{
					Right_Edge[j] = i + 1;
					break;
				}
				else
				{
					Right_Edge[j] = Right_Line;

				}
			}
		}
		for ( j = Bottom_Line - 5; j > Top_Line+8; j--)
		{
			if(Right_Edge[j+1]-Right_Edge[j]>circle_point_judge&&
				abs(Right_Edge[j]-Right_Edge[j-1])<5&&
			    abs(Right_Edge[j-2]-Right_Edge[j-1])<5 &&
				Right_Edge[j]<Right_Line-20 /*&&
				Right_Edge[j]-Left_Edge[j]<Road_Width_P10[j]*/)
			{
				circle_Y = Right_Edge[j];
				circle_X = j;
				break;
			}
		}
	}
	//左环
	if (circle_direction == 1)
	{
		Right_Edge[Bottom_Line] = Right_Black_boundary[Bottom_Line];
		//向右搜线
		for ( j = Bottom_Line - 1; j > Top_Line; j--)
		{
			for ( i = Right_Edge[j + 1] - 10; i < Right_Line - 0; i++)
			{
				if (i > Left_Line && image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i + 1] == White && image_IterativeThSegment[j][i + 2] == Black /*&& image_binaryzation[j][i + 3] == 1*/) //即中间向右边搜线，遇到白白黑黑的时候为边界
				{
					Right_Edge[j] = i + 1;
					break;
				}
				else
				{
					Right_Edge[j] = Right_Line;
				}
			}
		}
		//用右搜左
		for ( j = Bottom_Line ; j > Top_Line; j--)
		{
			for ( i = Right_Edge[j]; i > Left_Line + 2; i--)
			{
				if (image_IterativeThSegment[j][i] == White && image_IterativeThSegment[j][i - 1] == White && image_IterativeThSegment[j][i - 2] == Black)
				{
					Left_Edge[j] = i - 1;
					break;
				}
				else
				{
					Left_Edge[j] = Left_Line;
				}
			}
		}
		for ( j = Bottom_Line - 5; j > Top_Line+2; j--)
		{
			if(Left_Edge[j]-Left_Edge[j+1]>circle_point_judge&&
				abs(Left_Edge[j]-Left_Edge[j-1])<5&&
			   abs(Left_Edge[j-2]-Left_Edge[j-1])<5 &&
				Left_Edge[j]>Left_Line+20 /*&&
				Right_Edge[j]-Left_Edge[j]<Road_Width_P10[j]*/
				)
			{
				circle_Y = Left_Edge[j];
				circle_X = j;
				break;
			}
		}
	}
}