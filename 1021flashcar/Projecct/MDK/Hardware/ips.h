#include "fsl_common.h"

#ifndef _IPS_H_
#define _IPS_H_

//#include "Headfile.h"
//#include "oledfont.h"
//#include "fsl_lpspi.h"
//#include "fsl_gpio.h"
//#include "fsl_iomuxc.h"

#define LCD_W 240
#define LCD_H 240



#define IPS_RES_L() gpio_set(IPS_REST_PIN,0)
#define IPS_RES_H() gpio_set(IPS_REST_PIN,1)

#define IPS_DC_L() gpio_set(IPS_DC_PIN,0)
#define IPS_DC_H() gpio_set(IPS_DC_PIN,1)
#define IPS_SPIN        SPI_1           //定义使用的SPI号
#define IPS_SCL         SPI1_SCK_B10    //定义SPI_SCK引脚
#define IPS_SDA         SPI1_MOSI_B12   //定义SPI_MOSI引脚
#define IPS_SDA_IN      SPI1_MISO_B13   //定义SPI_MISO引脚  IPS没有MISO引脚，但是这里任然需要定义，在spi的初始化时需要使用
#define IPS_CS          SPI1_CS0_B11    //定义SPI_CS引脚
     
#define IPS_BL_PIN      B13	            //液晶背光引脚定义  由于实际通讯未使用B13因此 这里复用为GPIO控制BL引脚
#define IPS_REST_PIN    B8              //液晶复位引脚定义
#define IPS_DC_PIN 	    B9	            //液晶命令位引脚定义

#define IPS_DC(x)       gpio_set(IPS_DC_PIN,x);
#define IPS_REST(x)     gpio_set(IPS_REST_PIN,x);

void Lcd_Init(void); 
void IPS_DisplayGrayImage(void);
void LCD_Clear(uint16_t Color);
void Address_set(unsigned int x1,unsigned int y1,unsigned int x2,unsigned int y2);
void LCD_WR_DATA8(uint8_t da); //发送数据-8位参数
void LCD_WR_DATA(int da);
void LCD_WR_REG(uint8_t da);

void LCD_DrawPoint(uint16_t x,uint16_t y);//画点
void LCD_DrawPoint_big(uint16_t x,uint16_t y);//画一个大点
uint16_t  LCD_ReadPoint(uint16_t x,uint16_t y); //读点
void Draw_Circle(uint16_t x0,uint16_t y0,uint8_t r);
void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);		   
void LCD_Fill(uint16_t xsta,uint16_t ysta,uint16_t xend,uint16_t yend,uint16_t color);
void LCD_ShowChar(uint16_t x,uint16_t y,uint8_t num,uint8_t mode);//显示一个字符

void LCD_ShowNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len);//显示2个数字
void LCD_ShowString(uint16_t x,uint16_t y,const uint8_t *p);		 //显示一个字符串,16字体
void LCD_Displayimage032_gray(uint16_t *p) ;
void LCD_display_Debug(void);
void LCD_ImageDisplay(void);
void LCD_display(void);
void LCD_display_RunMode(void);
void LCD_display0(void);
void LCD_display1(void);
void LCD_display2(void);
void LCD_display3(void);
void LCD_WriteString(uint16_t col,uint16_t row,const uint8_t *p);
void LCD_CursorFill(uint16_t col,uint16_t row,uint16_t width,uint16_t high,uint16_t color);
void LCD_RecordLine(void);

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GRAY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
 
#define LIGHTGREEN     	 0X841F //浅绿色
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)

extern uint16_t BACK_COLOR,POINT_COLOR; //背景色和画笔色

#endif
