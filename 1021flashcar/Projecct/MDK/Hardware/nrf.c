#include "nrf.h"
#include "servo.h"
#include "encoder.h"
#include "global.h"


extern int encoder_l1,encoder_r1;
void ANO_DT_Send_MotoPWM(uint16 m_1,uint16 m_2,uint16 m_3,uint16 m_4,uint16 m_5);
void ANO_DT_Send_Servo(uint16 m_1);
uint8 str[100];
void niming_report_SENSER(void)
{
	
//	ANO_DT_Send_Senser(L_speed,speedl_set,R_speed,speedr_set,serve-760,Fuzzy_Servo.Pos*100,Fuzzy_Servo.Dpos*100,middle_error*10,dmiddle_error*10);//前9条int16波形
//	
//	ANO_DT_Send_Status(speedl_set,L_speed,0,10);//10-12float波形 13 14和15没有
//	
//	ANO_DT_Send_MotoPWM(16,17,18,19,20);//16-20为 u16波形
	
	//sprintf只用0.4ms
	//115200串口发送得7ms
	//波特率为1382400时下面2条加起来共为1ms
//	sprintf(str,
//					"d:%.3f,%.3f,%d,%d,%d,%.3f,%d,%d,%d,%.3f,%.3f,%.3f,%.3f,%.3f,%d,%d,%d,%d,%d\n",
//					 Fuzzy_Servo.Dpos,Fuzzy_Servo.Pos,L_speed,speedl_set,L_out,
//					 middle_error,R_speed,speedr_set,R_out,dmiddle_error,
//					 angle,serve,fil_dmiddle_error,EDS_Delta,time3,
//					 Encoder_block_Sum,Encoder_block_L,Encoder_block_R,Block_out);
	
//	sprintf(str,"d:%d,%d,%d,%d,%d,%.3f\r\n",Encoder_block_Sum,Encoder_block_L,Encoder_block_R,time4,time5,Status_Angle);
//	sprintf(str,"%d  %d\r\n",accelerate_flag,(L_speed+R_speed)/2);
//	sprintf(str,"%d, %d,   %d, %d,   %d, %d,   %d, %d,   %f \r\n",EM_middle_value,EM_circle_flag,road_lost_flag,road_lost_direction,circle_process,cross_flag,serve_choose_flag,cycle_time,speed_final);
//	sprintf(str,"%d,   %d, %d, %d, %.1f, %.1f, %d,a \r\n",EM_middle_value,EM_circle_flag,serve_choose_flag,cycle_time,speed_final,serve,EM_out);
	              //sprintf(str,"  %d, %d \r\n",encoder_l1,encoder_r1);
//	sprintf(str,"d:%d,%d,%d,%d,%d,%d\r\n",L_speed ,R_speed,(L_speed+R_speed)/2,speedl_set,speedr_set,(speedr_set+speedl_set)/2-(L_speed+R_speed)/2);
	               //uart_putstr(USART_6,"a"); 
//								ANO_DT_Send_Servo(servo_control);
     uint8 dat=0;
     uart_query(USART_6,&dat);
		 if(dat=='5') Mode=0;
		 
     int a=0,b=0,c=0;
		 ANO_DT_Send_MotoPWM(speedl_set*1000,speedr_set*1000,L_error*1000,R_error*1000,Dif_array[0]*1000);
						 
//								 sprintf(str,"  A  \r\n");
//				         uart_putstr(USART_6,str);
	
}
#define BYTE0(dwTemp)       ( *( (char *)(&dwTemp)	  ) )
#define BYTE1(dwTemp)       ( *( (char *)(&dwTemp) + 1) )
#define BYTE2(dwTemp)       ( *( (char *)(&dwTemp) + 2) )
#define BYTE3(dwTemp)       ( *( (char *)(&dwTemp) + 3) )
uint8 data_to_send[50];	//发送数据缓存
void ANO_DT_Send_Senser(int16 a_x,int16 a_y,int16 a_z,int16 g_x,int16 g_y,int16 g_z,int16 m_x,int16 m_y,int16 m_z)
{
	uint8 _cnt=0;
	vint16 _temp;
	
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0x02;
	data_to_send[_cnt++]=0;
	
	_temp = a_x;
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = a_y;
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = a_z;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	
	_temp = g_x;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = g_y;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = g_z;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	
	_temp = m_x;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = m_y;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = m_z;	
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	
	data_to_send[3] = _cnt-4;
	
	uint8 sum = 0;
	for(uint8 i=0;i<_cnt;i++)
		sum += data_to_send[i];
	data_to_send[_cnt++] = sum;
	
//	ANO_DT_Send_Data(data_to_send, _cnt);
	uart_putbuff(USART_6,data_to_send,_cnt);
}
void ANO_DT_Send_Status(float angle_rol, float angle_pit, float angle_yaw, int32 alt)
{
	uint8 _cnt=0;
	vint16 _temp;
	vint32 _temp2 = alt;
	
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0x01;
	data_to_send[_cnt++]=0;
	
	_temp = (int16)(angle_rol*100);
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = (int16)(angle_pit*100);
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	_temp = (int16)(angle_yaw*100);
	data_to_send[_cnt++]=BYTE1(_temp);
	data_to_send[_cnt++]=BYTE0(_temp);
	
	data_to_send[_cnt++]=BYTE3(_temp2);
	data_to_send[_cnt++]=BYTE2(_temp2);
	data_to_send[_cnt++]=BYTE1(_temp2);
	data_to_send[_cnt++]=BYTE0(_temp2);

	
	data_to_send[3] = _cnt-4;
	
	uint8 sum = 0;
	for(uint8 i=0;i<_cnt;i++)
		sum += data_to_send[i];
	data_to_send[_cnt++]=sum;
	
//	ANO_DT_Send_Data(data_to_send, _cnt);
	uart_putbuff(USART_6,data_to_send,_cnt);
}
void ANO_DT_Send_MotoPWM(uint16 m_1,uint16 m_2,uint16 m_3,uint16 m_4,uint16 m_5)
{
	uint8 _cnt=0;
	
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0x06;
	data_to_send[_cnt++]=0;
	
	data_to_send[_cnt++]=BYTE1(m_1);
	data_to_send[_cnt++]=BYTE0(m_1);
	data_to_send[_cnt++]=BYTE1(m_2);
	data_to_send[_cnt++]=BYTE0(m_2);
	data_to_send[_cnt++]=BYTE1(m_3);
	data_to_send[_cnt++]=BYTE0(m_3);
	data_to_send[_cnt++]=BYTE1(m_4);
	data_to_send[_cnt++]=BYTE0(m_4);
	data_to_send[_cnt++]=BYTE1(m_5);
	data_to_send[_cnt++]=BYTE0(m_5);
//	data_to_send[_cnt++]=BYTE1(m_6);
//	data_to_send[_cnt++]=BYTE0(m_6);
//	data_to_send[_cnt++]=BYTE1(m_7);
//	data_to_send[_cnt++]=BYTE0(m_7);
//	data_to_send[_cnt++]=BYTE1(m_8);
//	data_to_send[_cnt++]=BYTE0(m_8);
	
	data_to_send[3] = _cnt-4;
	
	uint8 sum = 0;
	for(uint8 i=0;i<_cnt;i++)
		sum += data_to_send[i];
	
	data_to_send[_cnt++]=sum;

//	ANO_DT_Send_Data(data_to_send, _cnt);
	uart_putbuff(USART_6,data_to_send,_cnt);
}
//Pos和Dpos是确定的0-6的小数，放大100倍发匿名
//middle_error是大概的-60-60的整数.放大10倍发匿名





void ANO_DT_Send_Servo(uint16 m_1)
{
	uint8 _cnt=0;
	
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0xAA;
	data_to_send[_cnt++]=0x06;
	data_to_send[_cnt++]=0;
	
	data_to_send[_cnt++]=BYTE1(m_1);
	data_to_send[_cnt++]=BYTE0(m_1);
	
	
	data_to_send[3] = _cnt-4;
	
	uint8 sum = 0;
	for(uint8 i=0;i<_cnt;i++)
		sum += data_to_send[i];
	
	data_to_send[_cnt++]=sum;

//	ANO_DT_Send_Data(data_to_send, _cnt);
	uart_putbuff(USART_6,data_to_send,_cnt);
}